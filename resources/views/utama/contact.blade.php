@extends('layouts.main')

@section('content')
	<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/kl.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center text-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">
              

            <!-- </p>
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
            </p> -->
          </div> 
        </div>
      </div>
    
    </section>
    <!-- END section image -->
<!-- probootstrap-section-half d-md-flex" id="section-about" -->
    <section class="probootstrap_section" id="section-contact">
        <div class="container">
          <div style="width: 1200px; height: 500px;">
            {!! Mapper::render() !!}
          </div>
        </div>
    </section>

    <section class="probootstrap_section bg-light" id="section-contact">
      <div class="container">
        
        <div class="row">
          <div class="col-md-6 probootstrap-animate" id="section">
            <h3 class="mb-5">MORE INFORMATION</h3>
            <p>HEAD OFFICE</p>
            <div class="row">
              <div class="col-md-6">
                <ul class="probootstrap-contact-details">
                  <li>
                    <span class="text-uppercase"><span class="ion-ios-telephone"></span> Phone</span>
                    <p>(+60) 192662717</p>
                  </li>
                  <li>
                    <span class="text-uppercase"><span class="ion-ios-email"></span> Email</span>
                    <p>info@global-i.com</p>
                  </li>
                </ul>
              </div>
              <div class="col-md-6">
                <ul class="probootstrap-contact-details">
                  <li>
                    <span class="text-uppercase"><span class="ion-location"></span> Address</span>
                    <p>40-2-1, Jalan 6/18A, Taman Mastiara, off Batu 5, Jalan Ipoh, 51200 Kuala Lumpur</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6  probootstrap-animate">
            <form action="{{url('moreinfo#section')}}" method="post" class="probootstrap-form probootstrap-form-box mb60">
              {{ csrf_field()}}

              @if(Session::has('flash_message'))
                  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
              @endif
              
              <div class="row mb-3">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="fname" class="sr-only sr-only-focusable">First Name</label>
                    <input type="text" class="form-control" id="fname" name="first" placeholder="First Name" required autofocus>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lname" class="sr-only sr-only-focusable">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="last" placeholder="Last Name" required autofocus>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="sr-only sr-only-focusable">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required autofocus>
              </div>
              <div class="form-group">
                <label for="message" class="sr-only sr-only-focusable">Message</label>
                <textarea cols="30" rows="10" class="form-control" id="message" name="message" placeholder="Write your message" required autofocus></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-danger button-pointer waves-effect" data-type="success" id="submit" name="submit" value="Send Message" >
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- END section of alamat-->

@endsection