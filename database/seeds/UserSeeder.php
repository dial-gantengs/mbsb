<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('users')->insert([
			'name'            => 'Wakudiallah',
			'email'           => 'wakudiallah@netxpert.com.my',
			'password'        => bcrypt('netxpert123'),
			'role'            => 1,
			'activation_code' => 1,
        ]);
        DB::table('users')->insert([
			'name'            => 'IT Netxpert',
			'email'           => 'it@netxpert.com.my',
			'password'        => bcrypt('netxpert123'),
			'role'            => 1,
			'activation_code' => 1,
        ]);
        DB::table('users')->insert([
            'name'            => 'Admin1',
            'email'           => 'admin1@netxpert.com.my',
            'password'        => bcrypt('netxpert123'),
            'role'            => 2,
            'activation_code' => 1,
        ]);
        DB::table('users')->insert([
            'name'            => 'Admin2',
            'email'           => 'admin2@netxpert.com.my',
            'password'        => bcrypt('netxpert123'),
            'role'            => 3,
            'activation_code' => 1,
        ]);
    }
}

