<?php

namespace App\Http\Controllers\Attribute;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\RepliedToThread;
use App\Model\ContactInfo;
use Auth;

class MoreInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $message = ContactInfo::orderBy('created_at','desc')->get();

        return view('adminpage.attribute.index', compact("message"));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data          = new ContactInfo;
        $data->first   = $request->first;
        $data->last    = $request->last;
        $data->email   = $request->email;
        $data->message = $request->message;
        $data->save();

        auth()->user()->notify(new RepliedToThread()); 

        \Session::flash('flash_message','Message telah berjaya dihantar');
        
        return view('utama.contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
