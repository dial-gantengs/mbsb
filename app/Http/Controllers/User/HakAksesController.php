<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserGroup_param;
use Yajra\Datatables\Datatables;

class HakAksesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminpage.hakakses.index');
    }

    public function dataWorkgroup()
    {
        
            $usergroup = usergroup_param::select(['id','kod_workgroup','nama_workgroup','keterangan']);
            return Datatables::of($usergroup)
            ->addColumn('action', function ($usergroup) {
                return 
                    '<a href="'.route('user.hakakses.edit', ["id"=>$usergroup ]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit </a> 
                   
                   <a href="'. route('user.hakakses.destroy', ["id"=>$usergroup ]) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a> 
                   ' ;

            })
            ->make(true);
            
    }
    public function create()
    {
        return view('adminpage.hakakses.create');
    }

    
    public function store(Request $request)
    {
        $data = new UserGroup_param;
        $data->kod_workgroup = $request->kod_workgroup;
        $data->nama_workgroup = $request->nama_workgroup;
        $data->keterangan  = $request->keterangan;
        $data->save();
        
        return redirect()->route('user.hakakses.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $dataHakAkses = app("UserGroup_param")->findOrFail($id);
        return view('adminpage.hakakses.update', compact("dataHakAkses"));
    }

    public function update(Request $request, $id)
    {
        $data = UserGroup_param::find($id);
        $data->kod_workgroup = $request->kod_workgroup;
        $data->nama_workgroup = $request->nama_workgroup;
        $data->keterangan  = $request->keterangan;
        $data->save();

        return redirect()->route('user.hakakses.index');
    }

    public function destroy(Request $request)
    {
        //$data = usergroup_param::where('id',$id)->delete();                
        //return redirect('hakakses');
       
        app("UserGroup_param")->find($request->id)->delete();
        return redirect()->route('user.hakakses.index');
   
    }
}
