<?php

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
$breadcrumbs = array(
	"Home" => APP_URL
);

if(empty($reminders)) {
	$total_reminders = 0;
}
else {
	$total_reminders = $reminders;
}

if (!empty($user)) {
	if ($user->role=='1') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"Documents Rejected" => array(
				"title" => "Documents Rejected",
				"url" => url('/')."/docsreject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Rejected" => array(
				"title" => "Application Rejected",
				"url" => url('/')."/reject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Approved" => array(
				"title" => "Application Approved",
				"url" => url('/')."/approved",
				"icon" => "fa fa-check-square"
			),
			"Route Back Status" => array(
				"title" => "Route Back Status",
				"url" => url('/')."/routeback_status",
				"icon" => "fa fa-chevron-circle-left"
			),
			"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"bankpersatuan" => array(
						"title" => "Bank Persatuan",
						"icon" => "fa fa-bank",
						"sub" => array(
							"branchmaster" => array(
								"title" => "Branch Master",
								"url" => url('/')."/admin/allbranch/index",
								"icon" => "fa-share-alt"
							),
							"branchuser" => array(
								"title" => "Branch User",
								"url" => url('/')."/admin/allbranch_user/index",
								"icon" => "fa-users"
							),
							"hquser" => array(
								"title" => "HQ User",
								"url" => url('/')."/admin/allhq_user/index",
								"icon" => "fa-user"
							),
							
						)
					),
						"Employer" => array(
						"title" => "Employer",
						"icon" => "fa-suitcase ",
						"url" => url('/')."/employer",
					
					),

				)
			)	
			,
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),

						"registered_unsubmitdoc" => array(
						"title" => "Not Submit Documents",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered_unsubmitdoc",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),

					"submitted Application" => array(
						"title" => "Submitted",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),
					"insentif" => array(
						"title" => "Insentif",
						"icon" => "fa-file ",
						"url" => url('/')."/report/insentif",
					
					),

				)
			),
			"reminder" => array(
				"title" => "Reminder",
				"url" => url('/')."/reminder",
				"icon" => "fa fa-chevron-circle-left"
			)
		);
	}
	else if ($user->role=='4') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			
			"new_app_admin" => array(
				"title" => "Add New Applicant",
				"url" => url('/')."/admin_praapplication",
				"icon" => "fa-plus"
			),
			"app_agent" => array(
				"title" => "Application by Agent",
				"url" => url('/')."/app_agent",
				"icon" => "fa-home"
			),
			"Documents Rejected" => array(
				"title" => "Documents Rejected",
				"url" => url('/')."/docsreject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Rejected" => array(
				"title" => "Application Rejected",
				"url" => url('/')."/reject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Approved" => array(
				"title" => "Application Approved",
				"url" => url('/')."/approved",
				"icon" => "fa fa-check-square"
			),
			"Route Back Status" => array(
				"title" => "Route Back Status",
				"url" => url('/')."/routeback_status",
				"icon" => "fa fa-chevron-circle-left"
			),
			"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"bankpersatuan" => array(
						"title" => "Bank Persatuan",
						"icon" => "fa fa-bank",
						"sub" => array(
							"branchmaster" => array(
								"title" => "Branch Master",
								"url" => url('/')."/admin/allbranch/index",
								"icon" => "fa-share-alt"
							),
							"branchuser" => array(
								"title" => "Branch User",
								"url" => url('/')."/admin/allbranch_user/index",
								"icon" => "fa-users"
							),
							"hquser" => array(
								"title" => "HQ User",
								"url" => url('/')."/admin/allhq_user/index",
								"icon" => "fa-user"
							),
							
						)
					),
					
						"Agent" => array(
						"title" => "Agent",
						"icon" => "fa-users ",
						"url" => url('/')."/agent",
					
					),

					
						"email_marketing" => array(
						"title" => "Email Marketing",
						"icon" => "fa-users ",
						"url" => url('/')."/email_marketing",
					
					),
						"Employer" => array(
						"title" => "Employer",
						"icon" => "fa-suitcase ",
						"url" => url('/')."/employer",
					
					),

				)
			)	
			,
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),

						
						"registered_unsubmitdoc" => array(
						"title" => "Not Submit Documents",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered_unsubmitdoc",
					
					),

						"registered_submitdoc" => array(
						"title" => "Submitted Documents",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered_submitdoc",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),

						"rejectdocs" => array(
						"title" => "Rejected Documents",
						"icon" => "fa-file ",
						"url" => url('/')."/report/rejectdocs",
					
					),

						"submitted" => array(
						"title" => "Submitted Application to Netxpert",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
						"submitted_bank" => array(
						"title" => "Submitted Application to Bank",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted_bank",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),
					"insentif" => array(
						"title" => "Insentif",
						"icon" => "fa-file ",
						"url" => url('/')."/report/insentif",
					
					),
					"response" => array(
						"title" => "Response",
						"icon" => "fa-clock-o ",
						"url" => url('/')."/report/response",
					
					)


				)
			)
		);
	}

	else if ($user->role=='3') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"report" => array(
				"title" => "Report",
				"url" => url('/')."/report/insentif",
				"icon" => "fa-file"
			),
			"report" => array(
				"title" => "Report",
				"icon" => "fa-book",
				"sub" => array(	
						"insentif" => array(
						"title" => "Insentif",
						"icon" => "fa-money ",
						"url" => url('/')."/report/insentif",
					
					),
						
						"response" => array(
						"title" => "Response",
						"icon" => "fa-clock-o ",
						"url" => url('/')."/report/response",
					
					)
				)
					
			)
		);
	}
	else if ($user->role=='5') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"active_list" => array(
				"title" => "Agent",
				"url" => url('/')."/active_list",
				"icon" => "fa-bank"
			),
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),

						
						"registered_unsubmitdoc" => array(
						"title" => "Not Submit Documents",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered_unsubmitdoc",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),

						"rejectdocs" => array(
						"title" => "Rejected Documents",
						"icon" => "fa-file ",
						"url" => url('/')."/report/rejectdocs",
					
					),

						"submitted" => array(
						"title" => "Submitted Application",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),
					"insentif" => array(
						"title" => "Insentif",
						"icon" => "fa-file ",
						"url" => url('/')."/report/insentif",
					
					),

				)
			),
			
			"reminder" => array(
				"title" => "Reminder",
				"url" => url('/')."/admin/master/reminders",
				"icon" => "fa fa-lg fa-fw fa-comment-o",
				"icon_badge" => $total_reminders
			)		
		);
	}

	else if ($user->role=='6') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"New Applicant" => array(
				"title" => "Add New Applicant",
				"url" => url('/')."/agent_praapplication",
				"icon" => "fa fa-user"
			)

		);
	}

	else  {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			)
		);
	}
}

//configuration variables
$page_title = "";
$page_css = array();
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
$page_html_prop = array(); //optional properties for <html>
?>