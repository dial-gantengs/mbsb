<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Reset Page ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
        <div id="main" role="main">
<br><br><br><br>
            <!-- MAIN CONTENT -->
            <div id="content" class="container">
               
           
          
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                 
             

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                         <div class="well no-padding">
                


   {!! Form::open(['url' => 'password/reset','class' => 'smart-form client-form' ]) !!}
   
                
                        <header>
                  <p class="txt-color-white"><b>     Reset Password </b> </p>
            </header>

                        <fieldset>
                        
                   @if ($errors->any())
                    <div class="row">
                        <div class="alert alert-mini alert-danger margin-bottom-30 margin-left-20 margin-right-20"><!-- SUCCESS -->
                            <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                             @foreach ($errors->all() as $error)

                            {{ $error }}
                            @endforeach
                           
                            
                        </div>
                    </div>
                    @endif 
                        <!-- ALERT -->
                                            @if (session('status'))
                        
                         <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>           {{ session('status') }}</strong> 
    </div>

                    @endif
                                  @if (Session::has('message'))
    

   
    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif   

    {!! csrf_field() !!}
                         
                              <section>
                                <label class="label">Emel</label>
                                <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                    <input type="email" name="email" id="email" readonly  value="{{ $email or old('email') }}" >
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i></b></label>
                            </section>

                                  <input type="hidden" name="token" value="{{ $token }}">          
                            <section>
                                <label class="label">Password</label>
                                <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                    <input type="password" name="password" id="password" required="" >
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter your password </b></label>
                            </section>

                             <section>
                                <label class="label">Password Confirmation</label>
                                <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                    <input type="password" name="password_confirmation" id="password_confirmation"  required="" >
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter your password confirmation </b></label>
                            </section>

                           

                            
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                               Submit
                            </button>
                        </footer>
                  {!! Form::close() !!}  

                </div>
                    
                    </div>
                </div>
              
            </div>

        </div>
             
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body custom-scroll terms-body">
                        
 <div id="left">



            <h1>SMARTADMIN TERMS & CONDITIONS TEMPLATE</h1>



            <h2>Introduction</h2>

            <p>bla blas</p>



            </div>
            
            <br><br>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="button" class="btn btn-primary" id="i-agree">
                            <i class="fa fa-check"></i> I Agree
                        </button>
                        
                        <button type="button" class="btn btn-danger pull-left" id="print">
                            <i class="fa fa-print"></i> Print
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

        $(document).ready(function() {
                
                    $("#smart-form-register2").hide();
                $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        FullName: {
                            required : true
                        },
                        ICNumber : {
                            required : true
                        },
                        
                        PhoneNumber: {
                            required: true
                        },
                        Deduction: {
                            required: true
                        },
                        
                        Allowance: {
                            required: true
                        },
                        Package: {
                            required: true
                        },
                        Employment: {
                            required: true
                        },
                        Employer: {
                            required: true
                        },
                        
                        
                        BasicSalary: {
                            required: true
                        },
                        LoanAmount: {
                            required: true
                        }
                    },

                    // Messages for form validation
                    messages : {

                        FullName: {
                            required : 'Please enter your full name'
                        },
                        
                        ICNumber: {
                            required: 'Please select your ic number'
                        },
                        PhoneNumber: {
                            required: 'Please select your phone number'
                        },
                        Allowance: {
                            required: 'Please aneter yor allowance'
                        },
                        Deduction: {
                            required: 'Please enter your total deduction'
                        },
                        Package: {
                            required: 'Please select package'
                        },
                        Employment: {
                            required: 'Please select employement type'
                        },

                        Employer: {
                            required: 'Please select employer'
                        },


                        BasicSalary: {
                            required: 'Please select your basic salary'
                        },
                        LoanAmount: {
                            required: 'Please select your loan amount'
                        }
                    },

                    // Ajax form submition
                    submitHandler : function(form) {

                        $.ajax({

                            type: "POST",
                            dataType: 'json',
                            url: "/PersonalLoan/public/praapplication",
                            data: $('#smart-form-register').serialize(),

                            cache: false,
                            beforeSend: function () {
                                $('#response').html("loading...");
                            },

                            success: function () {
                                var Email = $('#Email2').val();
                                var FullName = $('#FullName').val()
                                var LoanAmount = $('#LoanAmount').val();
                                var Package = $("#Package option:selected").text();
                                
                                
                                
                                $("#LoanAmount2").val(LoanAmount);
                                $("#Package2").val(Package);
                                 $("#Email").val(Email);
                                 $("#FullName2").val(FullName);
                                $("#smart-form-register").hide();
                                $("#smart-form-register2").show();
                                $('#response').html(" ");
                                $("#MaxLoan").val(LoanAmount);                             
                               
                                $("#InterestRate").val("3.65");

                            },
                            error: function (xhr, status, error) {
                                var json = xhr.responseText,
                                         obj = JSON.parse(json);

                                alert(obj.ModelState[''][1]);


                                $('#response').html("");
                            }
                        });
                        return false;




                    },

                    // Do not change code below
                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());
                    }
                });

            });
    </script>

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>
<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
        

