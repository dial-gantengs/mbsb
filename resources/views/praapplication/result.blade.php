<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Get Egibity Result";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>




</style>
<!-- ==========================CONTENT STARTS HERE ========================== -->
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>

        <div id="main" role="main">
<br><br><br><br>

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
                     
             @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
           
          
				<div class="row">
					<div class="col-xs-11 col-sm-12 col-md-6 col-lg-6">
   
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">

<script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
    </script>
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_2_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_2_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_2_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_2_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_2_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
           ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        
            //responsive code end
        });
    </script>
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_3_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_3_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_3_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_3_slider = new $JssorSlider$("jssor_3", jssor_3_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_3_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_3_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
    </script>

    <style>
        
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('{{url('/')}}/img/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('{{url('/')}}/img/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
    </style>


    <div class="well hidden-xs" id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
           
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/blue.jpg">
          <img data-u="image" src="{{ url('/') }}/img/blue.jpg" />
        </a>
            </div>
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/purple.jpg">
          <img data-u="image" src="{{ url('/') }}/img/purple.jpg" />
        </a>
            </div>
            
            
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-05.jpg"> 
         <img data-u="image" src="{{ url('/') }}/img/WEB-05.jpg" />
        </a>
            </div>
              
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-07.jpg"> 
          <img data-u="image" src="{{ url('/') }}/img/WEB-07.jpg" />
        </a>
            </div>        
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>
    
	    <div class="well visible-xs" id="jssor_3" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
           
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/blue.jpg">
          <img data-u="image" src="{{ url('/') }}/img/blue.jpg" />
        </a>
            </div>
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/purple.jpg">
          <img data-u="image" src="{{ url('/') }}/img/purple.jpg" />
        </a>
            </div>
            
            
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-05.jpg"> 
         <img data-u="image" src="{{ url('/') }}/img/WEB-05.jpg" />
        </a>
            </div>
              
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-07.jpg"> 
          <img data-u="image" src="{{ url('/') }}/img/WEB-07.jpg" />
        </a>
            </div>
				  
		   <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo1.jpg"><img data-u="image" src="{{ url('/') }}/img/promo1.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo2.jpg"><img data-u="image" src="{{ url('/') }}/img/promo2.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo3.jpg"><img data-u="image" src="{{ url('/') }}/img/promo3.jpg" /></a>
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>
		 <br>
	    <div id="jssor_2" class="well hidden-xs"  style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">

        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
           
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo1.jpg"><img data-u="image" src="{{ url('/') }}/img/promo1.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo2.jpg"><img data-u="image" src="{{ url('/') }}/img/promo2.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo3.jpg"><img data-u="image" src="{{ url('/') }}/img/promo3.jpg" /></a>
            </div>

        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>  </div>
                        
                        
                    </div>

                    </div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="well no-padding">

							  @foreach($pra as $pra)

        <?php 
        $icnumber = $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
         $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;
        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10){ $durasi = 10 ;} 
        else { $durasi = $durasix ;}
 
        ?>
						
                                   <div class='smart-form client-form'id='smart-form-register2'>
                                <header> <p class="txt-color-white"><b>    Kelayakan Pembiayaan </b> </p> </header>
                               
                              
<form action='{{url('/')}}/praapplication/{{$id}}' method='post' enctype="multipart/form-data">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label"> <b>  Pakej </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="Package2" value="{{$pra->package->name}}"  name="Package2" placeholder="Package" disabled="disabled">
                                                <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                                 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                
                                                <b class="tooltip tooltip-bottom-right">Pakej</b>
                                            </label>
                                        </section>
                                        
                                        <section class="col col-6">
                                            <label class="label"> <b> <font color="red" size="2.5" > KELAYAKAN  PEMBIAYAAN  MAKSIMA  </font>  </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                 @foreach($loan as $loan)
                                                   <?php
                                            $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;

                                            $max  =  $salary_dsr * 12 * 10 ;
                                                                           

                                           if(!empty($loan->max_byammount)) 
                                                {
                                                  $ansuran = intval($salary_dsr)-1;
                                                  $bunga = 4.5/100;
                                                  $pinjaman = 0;


                                                  for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                                  $bungapinjaman = $i  * $bunga * $durasi ;
                                                  $totalpinjaman = $i + $bungapinjaman ;
                                                  $durasitahun = $durasi * 12;
                                                  $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
                                                  if ($ansuran2 > $ansuran &&  $ansuran2 < $ansuran or   $ansuran2  == $ansuran)
                                                          {
                                                              $pinjaman = $i;
                                                          }
                                                    
                                                  }   
                                                  if($pinjaman > 1) {
                                                      function pembulatan($uang) {
                                                          $puluhan = substr($uang, -3);
                                                          if($puluhan<500) {
                                                            $akhir = $uang - $puluhan; 
                                                          } 
                                                          else {
                                                            $akhir = $uang - $puluhan;
                                                          }
                                                          return $akhir;
                                                        }
                                                        // echo "Pinjaman".$pinjaman;
                                                        
                                                      $bulat = pembulatan($pinjaman);
                                                      $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                                      $loanz = $bulat;
                                                  }
                                                  else {
                                                    $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                                    $loanz = $loan->max_byammount;

                                                  }
                                              
                                             }
                                              else { $loanx =  number_format($loan->max_bysalary * $total_salary, 0 , ',' , ',' ) ; 
                                                $loanz = $loan->max_bysalary * $total_salary;
                                                if ($loanz > 199000) {

                                                      $loanz  = 200000;
                                                      $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                                }


                                            }

                                                 ?>
                                                @endforeach

                                                <input readonly type="text" id="MaxLoan"  
                                                value=" RM {{$loanx}}"name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                                 class="merah" requierd> 
                                                    <input readonly type="hidden" id="maxloanz"  
                                                value="{{$loanz}}"name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                            </label>


                                        </section>
                                        
                                       
                                    </div>
                                    <div class="row">
                                    <section class="col col-6">
                                            <label class="label"><b>  Jumlah Pembiayaan (RM) </b> </label>
                                            <label class="input state-<?php if( $pra->loanamount <= $loanz ) { print "success"; } else { print "error"; }?>">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{ $pra->loanamount }}" name="LoanAmount2" placeholder="RM " onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan</b>
                                            </label>


                                        </section>
                                        
                                         <section class="col col-6">
                                            <label class="label"> <b>  Jumlah Pendapatan </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="jumlahpendapatan" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="jumlahpendapatan" placeholder="Loan Amount" disabled="disabled">
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </label>
                                        </section>
                                         <section class="col col-6">
                                            <label class="label"> <b>  Ansuran Maksima </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="ansuranmaksima" value="RM {{ number_format($salary_dsr, 0 , ',' , ',' )  }}   / month" name="ansuranmaksima" placeholder="Loan Amount" disabled="disabled">
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                            </label>
                                            
                                        </section>
                                         <section class="col col-6">
                                            <label class="label"> &nbsp; </label>
                                             <footer>
                                             <button class="btn btn-danger " type="submit">
                                    <b> Kira Semula </b>
                                </button>
                                            
                                        </footer>

                                        </section>


                                        
                                        </div>
                                       
                                </fieldset>
                                 
                              </form> 
                                <fieldset>

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td valign="middle"> <b>  Tempoh </b> </td>
                                                <td class="hidden-xs"> <b>  Jumlah Pembiayaan  </b> </td>
                                                <td><b>  Ansuran Bulanan </b> </td>
                                                 <td width="40"> <b>  Kadar  Keuntungan </b> </td>
                                                   <td> <b>  Pilih </b> </td>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php if( $pra->loanamount <= $loanz ) {?>
                                            @foreach($tenure as $tenure)
                                            <?php 

                                                   $bunga2 =  $pra->loanamount * $tenure->rate /100   ;
                                                   $bunga = $bunga2 * $tenure->years;
                                                   $total = $pra->loanamount + $bunga ;
                                                   $bulan = $tenure->years * 12 ;
                                                   $installment =  $total / $bulan ;

                                                   

                                                   if($installment  <= $salary_dsr ) {
                                                  
                                                ?>
                                            <tr>
                                                <td><span class="visible-lg visible-md">{{$tenure->years}} years</span><span class="visible-xs visible-sm">{{$tenure->years}} <font size="2">years</font></span></td>
                                                <td class="hidden-xs"> RM {{ number_format( $pra->loanamount, 0 , ',' , ',' )  }}  </td>
                                                
                                                <td>RM {{ number_format($installment, 0 , ',' , ',' )  }} /<font size="2">bln</font>  </td>
                                              
                                                <td  align="center" >{{$tenure->rate}} %</td>
                                                <td align="center">  <input type="radio" name="tenure" class="tenure" value="{{$tenure->id}}" required> </td>
                                            </tr>
                                            <?php } ?>
                                            @endforeach
                                             <?php } ?>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    

                                <footer>
                              
                                  <button type="submit" class="btn btn-primary btn-lg" id="submit_tenure">
                                    <b>  Teruskan </b>
                                  </button>
                                 


                                   
                                  
                                </footer>

                    <div>
                    </form>
                                      

						</div>
					
					</div>
				</div>
			</div>
		

		</div>
		
		<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">

					
						
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        
                            <b><u> Konsep Pembiayaan </u></b>
                            <ul>
                            <li>
    Pembiayaan <b> i-Lestari </b>  adalah berkonsepkan  <b> Tawarruq </b> </li>

                            </ul>

                            <br>
                            <b><u>Kategori Pelanggan </u></b> 
                            <br>
                             1. Persendirian-i Lestari Awam 
                            <ul>
                            <li> Sektor Kerajaan dan Badan Berkanun <b> berstatus tetap </b>  dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan <b>  status kontrak bersambung </b> tidak kurang 2 tahun berkhidmat hanya kepada <b> Kakitangan KEMAS dan Jabatan Perpaduan sahaja. </b> </li>
                        <li>Anak Syarikat Kerajaan / GLC </li>
                            </ul>
                              2.    i-Lestari Swasta 
                             <ul>
                             <li>
                             Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)
                             </li>
                             </ul>

                                  <b><u> Jumlah dan Had Pembiayaan</u></b> <br><br>
<table style="BORDER-COLLAPSE: collapse" border="1" cellspacing="1" width="100%">

<tr>
<td colspan="2" align="center" bgcolor="#e8a74c" width="434">
<span style="LETTER-SPACING: 1pt; FONT-WEIGHT: 700">
<font style="FONT-SIZE: 9pt" face="Arial">Pakej</font>
</span>
</td>
<td align="center" bgcolor="#e8a74c">
<font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt; FONT-WEIGHT: 700" face="Arial">Had Pembiayaan</font>
</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#c74e59" height="19" width="434"><span style="LETTER-SPACING: 1pt"><font style="FONT-SIZE: 9pt" color="#ffffff" face="Arial">Persendirian i-Lestari Awam</font></span><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial"> (Kerajaan / Badan Berkanun / GLC)</font></td>
<td align="center" bgcolor="#c74e59" height="19"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">RM 200,000.00</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Persendirian i-Lestari Swasta Panel Majikan</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Persendirian i-Lestari Swasta Bukan Panel Majikan</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">(Tertakluk Kepada Had Maksima RM 200,000.00)</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : RM3,000.00 - RM 5,000.00</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">TIADA</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">8 Kali Gaji Kasar Sebulan</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : RM5,001.00 - RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : RM5,001.00 - RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">10 Kali Gaji Kasar Sebulan</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : ≥ RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : ≥ RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">15 Kali Gaji Kasar Sebulan</font></td></tr></table>
	
<br>
                            <b><u>Kadar Keuntungan </u></b> 
                            <br>
                             1. Persendirian-i Lestari Awam 
                            <ul>
                            <li> 1 - 3 tahun = 3.90% setahun   </li>
                        <li>4 - 10 tahun = 4.50% setahun </li>
                            </ul>
                              2.    i-Lestari Swasta Panel Majikan (Kadar Tetap)
                            <ul>
                            <li> Pendapatan RM 3,000.00 - RM 5,000.00 = 7.00% setahun   </li>
                        <li>Pendapatan RM 5,001.00 - RM 10,000.00 = 6.25% setahun  </li>
                        <li>Pendapatan > RM 10,001.00 = 6.00% setahun </li>
                            </ul>

                              3.    i-Lestari Swasta Bukan Panel Majikan (Kadar Tetap)
                            <ul>
                            <li> Pendapatan RM5,001 - RM10,000 = 6.50% Setahun   </li>
                      
                        <li>Pendapatan > RM10,000 = 6.25% Setahun </li>
                            </ul>
                              <b>  (Gaji Kasar = Gaji Pokok + Elaun Tetap)</b>
                              <br>
                              <br>
                              <b><u>Laporan CCRIS</u></b> 
                           
                            <ul>
                            <li> 
    Laporan CCRIS (dari aplikasi 'Credit Bureau') perlu dikemukakan untuk setiap permohonan.    </li>
                     

<table style="BORDER-COLLAPSE: collapse" align="center" border="1" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#e8a74c" width="129"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Tunggakan</font></b></span></td>
<td align="center" bgcolor="#e8a74c" width="226"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Kelulusan</font></b></span></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="129"><span style="LETTER-SPACING: 1pt"><font style="FONT-SIZE: 9pt" color="#ffffff" face="Arial">1 Bulan</font></span></td>
<td align="center" bgcolor="#c74e59" width="226"><span style="LETTER-SPACING: 1pt"><font style="FONT-SIZE: 9pt" color="#ffffff" face="Arial">Kelulusan secara budibicara</font></span></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="129"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">2 Bulan</font></td>
<td align="center" bgcolor="#c74e59" width="226"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Perlu menyatakan asas kelulusan dan mendapat kelulusan tertinggi</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="129"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">3 Bulan</font></td>
<td align="center" bgcolor="#c74e59" width="226"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Permohonan ditolak</font></td></tr></tbody></table>
                            </ul>
                            <br>
                            
                              <b><u>Tempoh Pembiayaan</u></b> 
                           
                            <ul>
                            <li> Maksimum 10 tahun (120 Bulan) atau sehingga umur persaraan atau yang mana lebih awal. 
                             </li></ul>
							
						<br>
                            
                             

                           
						</div>
                        
                        
					</div>

					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<br>
					<b><u>Nisbah Khidmat Hutang / DSR</u></b>  <br> <br>
                           
                           
<table style="BORDER-COLLAPSE: collapse" border="1" cellspacing="1" width="100%">
<tbody>
<tr>
<td rowspan="2" align="center" bgcolor="#e8a74c" width="218"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Kategori Pemohon</font></b></span></td>
<td colspan="2" align="center" bgcolor="#e8a74c" width="337"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Kadar DSR</font></b></span></td></tr>
<tr>
<td align="center" bgcolor="#e8a74c" width="269"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial">Pendapatan Bulanan</font></td>
<td align="center" bgcolor="#e8a74c" width="64"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial">DSR</font></td></tr>
<tr>
<td rowspan="4" align="center" bgcolor="#c74e59" width="218">
<p style="LINE-HEIGHT: normal; MARGIN-BOTTOM: 0pt" class="MsoNormal" align="center"><font color="#ffffff"><span style="FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">Kakitangan </span><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">Kerajaan &amp; GLC (Bayaran ansuran BPA &amp; PGM)</span></font></p>
<p></p></td>
<td align="center" bgcolor="#c74e59" width="269">
<p style="TEXT-ALIGN: center; LINE-HEIGHT: normal; MARGIN-BOTTOM: 0pt" class="MsoNormal" align="center"><font color="#ffffff"><span style="FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">&lt; RM2,000.00 </span></font></p><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">(Tertakluk kepada kelulusan Ibu Pejabat)</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">50%</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">RM2,000.00 – RM3,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">60%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM3,001.00 – RM 5,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">70%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM 5,001.00 dan ke atas</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">75%</span></font></td></tr>
<tr>
<td rowspan="4" align="center" bgcolor="#c74e59" width="218"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">Kakitangan Swasta &amp; Panel Majikan (Bayaran Ansuran / BPA / PGM / PD Cek / Arahan Tetap)</span></font></td>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM3,000.00 – RM5,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">60%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM5,001.00 – RM7,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">70%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM7,001.00 – RM10,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">75%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><font face="Arial"><span style="LINE-HEIGHT: 115%; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">≥</span></font><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM10,001 ke atas</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">80%</span></font></td></tr>
<tr>
<td rowspan="3" align="center" bgcolor="#c74e59" width="218"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">Kakitangan Swasta Bukan Panel Majikan (Bayaran Ansuran / BPA / PGM / PD Cek / Arahan Tetap)</span></font></td>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM5,001.00 – RM7,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">70%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM7,001.00 – RM10,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">75%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><font face="Arial"><span style="LINE-HEIGHT: 115%; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">≥</span></font><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM10,001 ke atas</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">80%</span></font></td></tr></tbody></table>

<br>
                            
                              <b><u>Syarat Jaminan</u></b> 
                           
                            <ul>
                            <li> 
    Penjamin / cagaran tidak diperlukan termasuk kakitangan dengan status kontrak bersambung (bagi kakitangan Kemas & Jabatan Perpaduan sahaja) 
                             </li></ul>
                            <br>
                            
                              <b><u>Perlindungan DTT</u></b> 
                           
                            <ul>
                            <li> Skim Takaful Keluarga Berkelompok</li></ul>

                             <br>
                            
                              <b><u>Bayaran Awal / Pendahuluan Pembiayaan </u></b> 
                           
                            <ul>
                            <li> Tiada bayaran awal / pendahuluan untuk kakitangan kerajaan, badan berkanun dan GLC dengan cara bayaran balik melalui potongan BPA.  
                             </li>
                             <li>Untuk lain-lain cara bayaran dan kategori majikan, bayaran awal / pendahuluan 1 bulan diambil / ditolak secara manual dari jumlah pembiayaan dan dikreditkan ke dalam akaun pembiayan yang berkenaan.</li></ul>
							
                            <br>
                            
                              <b><u>Had Pendedahan Pembiayaan </u></b> <br><br>

<table style="BORDER-COLLAPSE: collapse" border="1" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#e8a74c" width="130"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial"><b>Pelanggan</b></font></td>
<td align="center" bgcolor="#e8a74c" width="120"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial"><b>Bilangan Akaun</b></font></td>
<td align="center" bgcolor="#e8a74c"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial"><b>Jumlah Pembiayaan</b></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="130"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Baru / Sedia Ada</font></td>
<td align="center" bgcolor="#c74e59" width="120"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">3(tiga) Akaun</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">RM 200,000.00</font></td></tr></tbody></table>

<br>
                            
                              <b><u>Yuran Proses / Perkhidmatan </u></b> 
                           
                            <ul>
                            <li> Tiada yuran proses untuk pemohon daripada sektor kerajaan, badan berkanun dan GLC.</li> <li>
Yuran proses sebanyak 0.5% untuk kategori majikan swasta.</li> </ul>

<br>
                            
                              <b><u>Tempoh Pengeluaran</u></b> 
                           
                            <ul>
                        <li> Pengeluaran pembiayaan dalam tempoh 3(tiga) hari bekerja selepas penerimaan borang.</li></ul>


			</div>

		</div>
        </div>
    </div>
           <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>


		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Pendaftaran</h4>
                    </div>
                    <div class="modal-body">
        {!! Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                      <fieldset>
                        <input type="hidden" name="idpra" value='{{$pra->id}}'>

                      
                                      
                                   @endforeach  
                                    @if (!(empty(Auth::user())))
                                        @if($user->role==5)

                                        <section >
                                          <label class="label"><br>Emel</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="Email" id="Email" name="Email" value="{{$pra->email}}" placeholder="Email">
                                                <b class="tooltip tooltip-bottom-right">Emel</b>
                                            </label>
                                        </section>
                                          <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >
                                          <input type="hidden" name="via" value='1'>
                                          <input type="hidden" name="password" value='12345678'>
                                          <input type="hidden" name="password_confirmation" value='12345678'>
                                        


                                        @else

                                          <!-- Auth User except user role 5-->
                                          <section >
                                          <label class="label"><br>Emel</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="Email" id="Email" name="Email" value="" placeholder="Email">
                                                <b class="tooltip tooltip-bottom-right">Emel</b>
                                            </label>
                                        </section>
                                          <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >

                                          <section >
                                            <label class="label">Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password" name="password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">Password</b>
                                            </label>

                                        </section>
                                        <section >
                                            <label class="label">Pengesahan Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" >
                                                <b class="tooltip tooltip-bottom-right">Pengesahan Kata Laluan</b>
                                            </label>
                                        </section>




                                        @endif

                                    @else 
                                    <!-- Guest User -->
                                    <section >
                                          <label class="label"><br>Emel</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="Email" id="Email" name="Email" value="" placeholder="Email" >
                                                <b class="tooltip tooltip-bottom-right">Emel</b>
                                            </label>
                                        </section>
                                          <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >

                                        <section >
                                            <label class="label">Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password" name="password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">Password</b>
                                            </label>

                                        </section>
                                        <section >
                                            <label class="label">Pengesahan Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" >
                                                <b class="tooltip tooltip-bottom-right">Pengesahan Kata Laluan</b>
                                            </label>
                                        </section>
                                    @endif
                               
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      
                                     
                                </fieldset>
                       
                       
        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Batal
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
                                       Daftar
                                    </button>
                                   
                           {!! Form::close() !!}   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

		$(document).ready(function() {
				
				
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
					    FullName: {
							required : true
						},
						ICNumber : {
							required : true
						},
						
						PhoneNumber: {
						    required: true
						},
						Deduction: {
						    required: true
						},
						
						Allowance: {
						    required: true
						},
						Package: {
						    required: true
						},
						Employment: {
						    required: true
						},
						Employer: {
						    required: true
						},
						
						
						BasicSalary: {
						    required: true,
                            min : 2999
						},
						LoanAmount: {
						    required: true
						}
					},

					// Messages for form validation
					messages : {

					    FullName: {
							required : 'Please enter your full name'
						},
						
						ICNumber: {
						    required: 'Please enter your ic number'
						},
						PhoneNumber: {
						    required: 'Please enter your phone number'
						},
						Allowance: {
						    required: 'Please enter yor allowance'
						},
						Deduction: {
						    required: 'Please enter your total deduction'
						},
						Package: {
						    required: 'Please select package'
						},
						Employment: {
						    required: 'Please select employement type'
						},

						Employer: {
						    required: 'Please select employer'
						},


						BasicSalary: {
						    required: 'Please enter your basic salary',
                            min: 'minimum salary is 3000 my'
						},
						LoanAmount: {
						    required: 'Please enter your loan amount'
						}
					}
				});

			});
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                Email : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    Email2 : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                password : {
                    required : 'Please enter your password'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>

<script type="text/javascript">

$( ".tenure" ).change(function() {
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
     var _token = $('#token').val();
      var LoanAmount = $('#LoanAmount2').val();
       var maxloanz = $('#maxloanz').val();
      

  $.ajax({
                type: "PUT",
                url: '{{ url('/form/') }}'+'/8',
                data: { id_praapplication: id_praapplication, tenure: tenure, _token : _token, loanammount : LoanAmount, maxloan : maxloanz
                   
                },
                success: function (data, status) {

                

                }
            });

   
});
</script>

<script type="text/javascript">
    
     $(document).ready(function() {


    var id_praapplication = $('#id_praapplication').val();
     var _token = $('#token').val();
      var LoanAmount = $('#LoanAmount2').val();
       var maxloanz = $('#maxloanz').val();
      

  $.ajax({
                type: "PUT",
                url: '{{ url('/form/') }}'+'/9',
                data: { id_praapplication: id_praapplication,  _token : _token, loanammount : LoanAmount, maxloan : maxloanz
                   
                },
                success: function (data, status) {

                

                }
            });
     });

</script>

<?php 
	//include required scripts
	include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
		


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
  var tenure = $('input[name=tenure]:checked').val();
     
        if (tenure>0) { 
      $('#myModal').modal('show');
    }
    else{
      bootbox.alert('Sila Pilih Tempoh Pembiayaan !');
      
    }
    });
});

</script>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>