<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ContactInfo extends Model
{
    protected $table = 'contact_info';
    protected $fillable = [
       'first','last','email', 'message'
    ];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;
}