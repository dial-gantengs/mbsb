
<p>Hi,</p>

<p>We receive a request to change your password in ezlestari's website.</p>

<p>Click the following link to reset your password: <a href="{{ url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" target="_blank"><u>{{ url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}</u></a></p>

<p>Please ignore this email if you didn't request changing your password.</p>