@extends('layouts.main')

@section('content')
  <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/kl.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center text-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">
              

            <!-- </p>
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
            </p> -->
          </div> 
        </div>
      </div>
    
    </section>
    <!-- END section image -->

    <section class="probootstrap_section bg-light" id="section-contact">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">About Us</h2>
          </div>
        </div>
      </div>
    </section>  <!-- END section of about us-->

    <section class="probootstrap-section-half d-md-flex" id="section-about">
          
      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>
      <div class="probootstrap-text">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">
          
          <p class="heading text-justify mb-5">Global I Exceed Management Sdn.Bhd, (GIEM) adalah agensi berdaftar untuk Malaysia Building Society Berhad (MBSB) bagi tujuan pemasaran produk MBSB khususnya Produk Pembiayaan Peribadi I.</p>

          <p class="text-justify">Ditubuhkan sejak 2012, kami telah menaburkan khidmat perundingan kewangan peribadi melalui pembiayaan peribadi berlandaskan konsep Syariah. Kami telah membina jaringan rangkaian yang menghubungkan pelangan, institusi kewangan dan mana mana pihak yang berkaitan menerusi pengalaman pembiayaanasaran kami. Syarikat ini ditubuhkan dengan jayanya, berbekalkan semangat dan keazaman untuk bersaing dalam industri global.</p>
        </div>
      </div>
    </section><!-- END section -->

@endsection