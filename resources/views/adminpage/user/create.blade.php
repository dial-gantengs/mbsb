@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">person_add</i> User</a></li>
                        @if(Request::segment(3) === 'tambah')
                        <li class="active"><i class="material-icons">create</i> Create</li>
                        @elseif(Request::segment(3) == 'kemaskini')
                        <li class="active"><i class="material-icons">edit</i> Kemaskini</li>
                        @endif
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->

            <div class="row clearfix">
    			<div class="col-md-12">
    				<div class="card">
    					<div class="header bg-red">
    						<h2>Tambah</h2>
    					</div>

    					<div class="body">
    						<div class="row clearfix">
    							<form method="POST" action="@yield('url',route("user.user.create") )">
    								{{ csrf_field()}}
    
	                                <div class="col-sm-12">
	                                	<h5>Nama User :</h5>
	                                    <div class="form-group form-float">
	                                        <div class="form-line">
	                                        	<input type="text" class="form-control" name="name" value="@yield('name')" required autofocus>
	                                            <label class="form-label">Nama</label>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-sm-12">
	                                	<h5>Email :</h5>
	                                    <div class="form-group form-float">
	                                        <div class="form-line {{ $errors->has('email') ? ' has-error' : '' }}">
	                                        	<input type="email" class="form-control" name="email" value="@yield('email')" required autofocus>
	                                            <label class="form-label">Email</label>
	                                            @if ($errors->has('email'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('email') }}</strong>
				                                    </span>
				                                @endif
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-sm-12">
	                                	<h5>Password :</h5>
	                                    <div class="form-group form-float">
	                                        <div class="form-line">
	                                        	<input type="password" class="form-control" name="password" value="@yield('password')" required autofocus>
	                                            <label class="form-label">Password</label>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-sm-12">
	                                	<h5>Role :</h5>
	                                    <div class="form-group form-float">
	                                            <select class="form-control" name="role" required autofocus>
			                                        <option disabled selected>-- Please select --</option>
			                                        @foreach($usergroup as $role)
			                                        	<option value="{{$role->id}}">{{$role->nama_workgroup}}</option>
			                                        @endforeach
			                                    </select>
	                                    </div>
	                                </div>

	                                <input type="hidden" name="activation_code" id="activation_code" value="1" />

		                            <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
					                    <button type="submit" class="btn bg-red btn-block btn-lg waves-effect">
		                                    Simpan
		                                </button>
		                                <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button>
					                </div>
                                </form>
                            </div>
    					</div>
    				</div>
    			</div>
    			
    		</div>
        </div>
    </section>
@endsection