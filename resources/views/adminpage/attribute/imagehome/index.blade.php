@extends('vadmin.tampilan')

@section('content')
	<section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">build</i> Attribute</a></li>
                        <li class="active"><i class="material-icons">image</i> Image</li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->
        </div>
    </section>
@endsection