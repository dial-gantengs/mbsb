@extends('layouts.main')

@section('content')
<section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{asset('assets/images/kl.jpg')}}');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md">
                <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Lorem Ipsum is simply dummy text of</h2> 
                <p class="lead mb-5 probootstrap-animate">
                <!-- </p>
                <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
                </p> -->
            </div> 
            <div class="col-md probootstrap-animate">
                <form action="{{ route('password.email') }}" method="POST" class="probootstrap-form border border-danger">
                {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-md">
                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <h1>Password Reset</h1>
                                </div>
                                <div class="panel-body">
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                                            <div class="col-md-12 probootstrap-date-wrap">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md"></div>
                                            <div class="col-md">
                                                <button type="submit" class="btn btn-danger">
                                                    Send Password Reset Link
                                                </button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<footer class="probootstrap_section probootstrap-border-top">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-3">
          <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
          <ul class="list-unstyled">
            <li><a href="https://free-template.co" target="_blank">Home</a></li>
            <li><a href="https://free-template.co" target="_blank">About</a></li>
            <li><a href="https://free-template.co" target="_blank">Services</a></li>
            <li><a href="https://free-template.co" target="_blank">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
          <ul class="list-unstyled">
            <li><a href="https://free-template.co" target="_blank">Home</a></li>
            <li><a href="https://free-template.co" target="_blank">About</a></li>
            <li><a href="https://free-template.co" target="_blank">Services</a></li>
            <li><a href="https://free-template.co" target="_blank">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
          <ul class="list-unstyled">
            <li><a href="https://free-template.co" target="_blank">Home</a></li>
            <li><a href="https://free-template.co" target="_blank">About</a></li>
            <li><a href="https://free-template.co" target="_blank">Services</a></li>
            <li><a href="https://free-template.co" target="_blank">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
          <ul class="list-unstyled">
            <li><a href="https://free-template.co" target="_blank">Home</a></li>
            <li><a href="https://free-template.co" target="_blank">About</a></li>
            <li><a href="https://free-template.co" target="_blank">Services</a></li>
            <li><a href="https://free-template.co" target="_blank">Contact</a></li>
          </ul>
        </div>
      </div>
      <div class="row pt-5">
        <div class="col-md-12 text-center">
          <p class="probootstrap_font-14">&copy; 2017. All Rights Reserved. <br> Designed &amp; Developed by <a href="https://probootstrap.com/" target="_blank">ProBootstrap</a><small> (Don't remove credit link on this footer. See <a href="https://probootstrap.com/license/">license</a>)</small></p>
          <p class="probootstrap_font-14">Demo Images: <a href="https://unsplash.com/" target="_blank">Unsplash</a></p>
        </div>
      </div>
    </div>
  </footer>
@endsection
