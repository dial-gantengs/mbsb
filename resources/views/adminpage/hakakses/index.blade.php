@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">touch_app</i> Hak Akses</li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->
            
            <div class="row clearfix demo-button-sizes">
                <div class="col-md-10"></div>
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{!! route('user.hakakses.create') !!}" type="submit" class="btn bg-red btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Tambah
                    </a>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Data Hak Akses</h2>
                        </div>
                        <div class="body">
                            <table class="table" id="data-table">
                                <thead>
                                    <tr>
                                        <td width="10%">#</td>
                                        <td width="15%">Kod Workgroup</td>
                                        <td width="20%">Nama Workgroup</td> 
                                        <td width="30%">Keterangan</td>
                                        <td width="20%">Tindakan</td>             
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </section>

 <!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>


<script type="text/javascript">
    $(function(){
        $("#data-table").DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('user.hakakses.index.data') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'kod_workgroup', name: 'kod_workgroup' },
                { data: 'nama_workgroup', name: 'nama_workgroup' },
                { data: 'keterangan', name: 'keterangan' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection

