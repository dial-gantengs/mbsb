@extends('vadmin.tampilan')

@section('content')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
    

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">build</i> Attribute</a></li>
                        <li class="active"><i class="material-icons">send</i> Message</li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->
           
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Messaging Users</h2>
                        </div>
                        <div class="body">
                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Message</th>
                                        <th>Status</th>
                                        <th>Action</th>             
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php $i = 1; ?>

                                	@foreach($message as $data)
                                	<tr>
                                		<td></td>
                                		<td>{{$i++}}</td>
                                		<td>{{$data->last}}</td>
                                		<td>{{$data->email}}</td>
                                		<td>{{$data->message}}</td>
                                		@if($data->status == 0)
                                            <td><span class="label bg-green">For Reply</span></td> 
                                        @else
                                            <td><span class="label bg-orange">Replied</span></td>
                                        <td>{{$data->status}}</td>
                                        @endif
                                		@if($data->status == 0)
                                			<td><a href="#" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-send"></i> Reply </a> </td>
                                		@else
                                			<a href="#" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                		@endif
                                	</tr>
                                	
                                	@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	
	<script>
	    $(document).ready(function() {
	    $('#example').DataTable();
	    });
	</script>

	<script>
	    $(document).ready(function() {
	    $('#example1').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	            'csvHtml5',
	            'pdfHtml5'
	        ]
	    } );
	} );
	</script>

    
@endsection
