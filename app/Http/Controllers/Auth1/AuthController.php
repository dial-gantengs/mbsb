<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

     public function login(Request $request)
    {

        
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password ])) {


           // echo "Role = ".Auth::user()->role;

                // Authentication passed...

                if(Auth::user()->role==6 && (Auth::user()->active==0 || Auth::user()->active==2)) {
                 //if agent

                    Auth::logout();
                    return redirect('auth/login')->withErrors(['Your account is not approved']);
                }
                if(Auth::user()->role==6 && Auth::user()->first_login==1) {
                    return redirect()->intended('user');
                }

                return redirect()->intended('adminnew');
          
        }
        else {
            return redirect('auth/login')->withErrors(['Incorrect Email or Password']);
        }
                    
          
           
       
    }
}
