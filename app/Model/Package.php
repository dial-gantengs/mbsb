<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
   protected $hidden = ['created_at', 'updated_at','create_by'];
}
