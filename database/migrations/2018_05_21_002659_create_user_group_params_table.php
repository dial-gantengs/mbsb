<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('usergroup_param'))
            {
                Schema::create('usergroup_param', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('kod_workgroup');
                    $table->string('nama_workgroup',30);
                    $table->string('keterangan');
                    $table->SoftDeletes();
                    $table->timestamps();
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_group_params');
    }
}
