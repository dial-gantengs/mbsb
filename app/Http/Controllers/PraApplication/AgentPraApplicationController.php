<?php

namespace App\Http\Controllers\PraApplication;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\Employment;
use App\Model\PraApplication;
use App\Model\Loan;
use App\Model\Package;
use App\Model\Basic;
use App\Model\Contact;
use App\Model\Tenure;
use App\Model\Empinfo;
use App\Model\Document;
use App\Model\LoanAmmount;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Term;
use App\Model\Blocked_ic;
use App\Model\LoanCalculator;
use App\Model\LoanCalculator2;
use App\Http\Controllers\Controller;

use App\Model\Basic_v;
use App\Model\Contact_v;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount_v;
use App\Model\Spouse_v;
use App\Model\Reference_v;
use App\Model\Financial_v;
use App\Model\Document_v;

use Input;
use DateTime;
use Rhumsaa\Uuid\Uuid;
use App\Model\User;
use Auth;
use  Session;



class AgentPraApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user = Auth::user();
        if($user->role=='6' && (Auth::user()->active<>1)) {
             return redirect('/')->withErrors(['You dont have authorized to access the page. Agent is not approved']); 
         }

         $employment = Employment::get();
         $package = Package::all();
        return view('agent_praapplication.index', ['employment' => $employment, 'package' => $package, 'user' => $user  ]);
         
    }
	
	public function applynow()
    {
        $user = Auth::user();
         $employment = Employment::get();
         $package = Package::all();
        return view('agent_praapplication.applynow', ['employment' => $employment, 'package' => $package, 'user' => $user  ]);
         
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $praapplication = new PraApplication;
		$loancalculator = new LoanCalculator;
		$loancalculator2 = new LoanCalculator2;
        $basic = new Basic;
        $contact = new Contact;
        $empinfo = new Empinfo;
        $loanammount = new LoanAmmount;
        $spouse = new Spouse;
        $document = new Document;
        $reference = new Reference;
        $financial = new Financial;
        $term = new Term;
        $icnumber = $request->input('ICNumber');
        $employment  = $request->input('Employment');
         $employment2  = $request->input('Employment2');
        $basicsalary = $request->input('BasicSalary');
        $fullname_temp = $request->input('FullName');
        $fullname = strtoupper($fullname_temp);
        $phone = $request->input('PhoneNumber');
        $employer = $request->input('Employer');
        $employer2 = $request->input('Employer2');
        $allowance = $request->input('Allowance');
        $deduction = $request->input('Deduction');        
        $package = $request->input('Package');
        $majikan = $request->input('majikan');
        $loanAmount = $request->input('LoanAmount');
        $batas =  $allowance  +  $basicsalary ;
        
        $statuspekerjaan = $request->input('StatusPekerjaan');
        $kemas = $request->input('kemas');
        $masaberkhidmat = $request->input('MasaBerkhidmat');
       
        $blocked_ic = Blocked_ic::latest('created_at')->where('ic',  $icnumber )->count();

        
            if($employment=='1') {
                if($statuspekerjaan=="0") { // IF Kontrak
                    if($kemas<1) {
                        return redirect('/agent_praapplication')->with('message', "Untuk status kontrak, hanya kakitangan KEMAS dan Jabatan Perpaduan yang melebihi perkhidmatan 2 tahun layak memohon");
                    }
                }
            }
            else {
                if($masaberkhidmat<1) { // IF SWASTA KERJA LEBIH DARI 3 Tahun
                    return redirect('/agent_praapplication')->with('message', "Untuk kakitangan swasta, hanya pekerja yang telah berkhidmat melebihi 3 tahun layak memohon");
                }  
            }
        
       
        
         if($loanAmount < 1000)  {
          
             Session::flash('fullname', $fullname); 
             Session::flash('icnumber', $icnumber);
             Session::flash('phone', $phone); 
             Session::flash('basicsalary', $basicsalary); 
             Session::flash('allowance', $allowance);
             Session::flash('deduction', $deduction);
             Session::flash('loanAmount', $loanAmount);
             Session::flash('loanAmount2', $loanAmount);
             Session::flash('employer2', $employer2);
              Session::flash('employment', $employment);
             Session::flash('employment2', $employment2);
             if($employment  == '2' ){
              Session::flash('employer', $employer);
          }

             
             Session::flash('majikan', $majikan); 

                 
return redirect('/agent_praapplication')->with('message', "Jumlah pembiayaan minima RM  1,000 ");
        }


        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
         $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 

        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;

        if($umur >= 60) {

            return redirect('/agent_praapplication')->with('message', "Maaf umur melebihi had");

        }
        else {

            if($employment == 1  )  {   $had =  1999;  }
            else if ($employment == 3  )  {   $had =  5000;  } 
            else { $had = 2999; }
                if(  $batas > $had ) {

        
      
       
        $totalsalary = $basicsalary + $allowance   ;
        $id = Uuid::uuid4()->getHex(); 
        $praapplication->id   =  $id;
        $praapplication->fullname = $fullname;
        $praapplication->icnumber = $icnumber;
        $praapplication->phone = $phone;

        if(!empty($employer)) { 
        $praapplication->id_employer = $employer;
    }
    else {

         if($employment == 1  )  {     $praapplication->id_employer = 10;   }
            else if ($employment == 3  )  {    $praapplication->id_employer = 5;   } 

        

    }
        $praapplication->basicsalary = $basicsalary;
        $praapplication->allowance = $allowance;
        $praapplication->deduction = $deduction;
        $praapplication->id_package = $package;
        $praapplication->loanAmount = $loanAmount;
        $praapplication->majikan = $majikan;
        $praapplication->referral_id = Auth::user()->id;
        
        $praapplication->save();

        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 

		$loancalculator->id_praapplication   =  $id;
		$loancalculator->save();
		
		$loancalculator2->id_praapplication   =  $id;
		$loancalculator2->save();
                                                            

        $basic->id_praapplication   =  $id;
        $basic->new_ic = $icnumber;
        $basic->name =  strtoupper($fullname);
        $basic->dob =  $lahir ; 
        $basic->save();

        $contact->id_praapplication   =  $id;
         $contact->handphone   =  $phone;
        $contact->save();

        $empinfo->id_praapplication   =  $id;
        if(!empty($employer)) {
        $empinfo->employer_id = $employer; }
        else {
             $empinfo->employer_id = 5;
        }

         if($employment  != '2' ){
         $empinfo->empname = $majikan;
     }

         $empinfo->employment_id = $employment;
        $empinfo->salary = $basicsalary;
        $empinfo->allowance = $allowance;
        $empinfo->deduction = $deduction;
        $empinfo->save();

        $loanammount->id_praapplication   =  $id;
        $loanammount->loanammount   =  $loanAmount;
        $loanammount->save();


        $spouse->id_praapplication   =  $id;
        $spouse->save();

        $financial->id_praapplication   =  $id;
        $financial->save();

        $reference->id_praapplication   =  $id;
        $reference->save();

        $term->id_praapplication   =  $id;
        $term->referral_id = Auth::user()->id;
        $term->save();

        // verification

          $basic_v = new Basic_v ;
          $contact_v = new Contact_v ;
          $empinfo_v = new Empinfo_v ;
          $loanammount_v = new LoanAmmount_v ;
          $spouse_v = new Spouse_v ;
                $reference_v = new Reference_v ;
          $financial_v = new Financial_v ; 

        $contact_v->id_praapplication   =  $id;
        $basic_v->id_praapplication   =  $id;
        $empinfo_v->id_praapplication   =  $id;
        $loanammount_v->id_praapplication   =  $id;
        $spouse_v->id_praapplication   =  $id;
          
        $reference_v->id_praapplication   =  $id;
        $financial_v->id_praapplication   =  $id;
        $contact_v->save();
        $basic_v->save();
        $empinfo_v->save();
        $loanammount_v->save();
        $spouse_v->save();
                     
        $reference_v->save();
        $financial_v->save();

          return redirect('agent_praapplication/'.$id)->with('message', 'insert data succsess');
        }

        else {
             $had2 = number_format($had + 1, 0 , ',' , ',' ).'.00' ; 
             Session::flash('fullname', $fullname); 
             Session::flash('icnumber', $icnumber);
             Session::flash('phone', $phone); 
             Session::flash('basicsalary', $basicsalary); 
             Session::flash('allowance', $allowance);
             Session::flash('deduction', $deduction);
             Session::flash('loanAmount', $loanAmount);
              Session::flash('basicsalary2', $basicsalary); 
             Session::flash('employer2', $employer2);
              Session::flash('employment', $employment);
             Session::flash('employment2', $employment2);
             if($employment  == '2' ){
              Session::flash('employer', $employer);
          }

             
             Session::flash('majikan', $majikan); 

                 
return redirect('/')->with('message', "Syarat gaji minima RM  $had2 sebulan ");
        }

         
    }
    
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
        $user = Auth::user();
        $pra = PraApplication::where('id',$id)->limit('1')->get();
        $id_type= $pra->first()->employer->employment->id;
        $total_salary = $pra->first()->basicsalary + $pra->first()->allowance  ;
        $zbasicsalary = $pra->first()->basicsalary + $pra->first()->allowance;
        $zdeduction =  $pra->first()->deduction ;


        $loan = Loan::where('id_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

         $id_loan= $loan->first()->id;  

        $icnumber= $pra->first()->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

         $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   

    
       return view('agent_praapplication.result', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','user' ));
       
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pra = PraApplication::find($id);
        $loanammount = $request->input('LoanAmount2');
        $pra->loanammount = $loanammount;
        $pra->save();

         $loanx2 = LoanAmmount::where('id_praapplication',$id)->first();
        $loanx2->loanammount = $loanammount;
        $loanx2->save();

        return redirect('agent_praapplication/'.$id)->with('message', 'insert data succsess');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function update2(Request $request, $id)
    {
         $pra = PraApplication::find($id);
        $loanammount = $request->input('LoanAmount2');
        $pra->loanamount = $loanammount;
        $pra->save();

           
        $loanx2 = LoanAmmount::where('id_praapplication',$id)->first();
        $loanx2->loanammount = $loanammount;
        $loanx2->save();


        return redirect('agent_praapplication/'.$id)->with('message', 'insert data succsess');
    }
}
