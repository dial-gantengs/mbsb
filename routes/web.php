<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    
	Auth::routes();

	/*Tampilan Utama */
	
	Route::get('/', 'Utama\UtamaController@index');
	Route::get('/utama', 'Utama\UtamaController@index');
	Route::get('/about', 'Utama\UtamaController@about');
	Route::get('contact', 'Utama\UtamaController@contact');
	Route::get('service', 'Utama\UtamaController@service');
	Route::get('faq', 'Utama\UtamaController@faq');
	
	Route::post('moreinfo', 'Attribute\MoreInfoController@store');
	/*Route::get('reply/{id?}', 'Attribute\MoreInfoController@create');
	Route::post('reply', 'Attribute\MoreInfoController@send'); */


	Route::post('log', 'Auth\LoginController@loginrule');
	Route::get('/login1', function () {
    	return view('auth.login');
	});

	Route::get('/markAsRead',function(){
		auth()->user()->unreadNotifications->markAsRead();
	});


	/* Loan Menu */
	Route::get('/loan', 'Loan\LoanController@index');

	/* Admin Utama / Dashboard */
	Route::get('/adminnew', 'AdminNewController@index');


	Route::get('/commission', 'Commission\CommissionController@index');



	Route::group(['prefix'=>'userprofile' ], function () {
		/*    Profile		 */
		Route::group(['prefix' => 'profile'], function () {
			Route::get('/', ['as' => 'userprofile.profile.index', 'uses' => 'Profile\ProfileUserController@index']);
			Route::post('/tambah/{id?}', ['as' => 'userprofile.profile.insert', 'uses' => 'Profile\ProfileUserController@store']);

		});
	});


	Route::group(['prefix'=>'moreinfo' ], function () {
		/*   More Info @contact 	*/
		Route::group(['prefix' => 'customerinfo'], function () {
			Route::get('/', ['as' => 'moreinfo.index', 'uses' => 'Attribute\MoreInfoController@index']);
			Route::post('/tambah/{id?}', ['as' => 'moreinfo.profile.insert', 'uses' => 'Attribute\MoreInfoController@store']);

		});
	});


	/*   Upload Image Index @home 	*/
	Route::group(['prefix' => 'home'], function () {
		Route::get('/', ['as' => 'image.web', 'uses' => 'Attribute\ImageHomeController@index']);	
		Route::post('/tambah/{id?}', ['as' => 'image.home.insert', 'uses' => 'Attribute\ImageHomeController@store']);

	});
	

     /*Hak Akses*/
        Route::group(['prefix' => 'hakakses'], function () {
            Route::get('/', ['as' => 'user.hakakses.index', 'uses' => 'User\HakAksesController@index']);
            Route::get('/tambah', ['as' => 'user.hakakses.create', 'uses' => 'User\HakAksesController@create']);
            Route::get('/kemaskini/{id?}', ['as' => 'user.hakakses.edit', 'uses' => 'User\HakAksesController@edit']);
            Route::post('/tambah', ['as' => 'user.hakakses.store', 'uses' => 'User\HakAksesController@store']);
            Route::get('/padam/{id}', ['as' => 'user.hakakses.destroy', 'uses' => 'User\HakAksesController@destroy']);
            Route::post('/kemaskini/{id?}', ['as' => 'user.hakakses.update', 'uses' => 'User\HakAksesController@update']);
            Route::get('/dataWorkgroup', ['as' => 'user.hakakses.index.data', 'uses' => 'User\HakAksesController@dataWorkgroup']);
        });

        /*User*/
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', ['as' => 'user.user.index', 'uses' => 'User\UserController@index']);
            Route::get('/tambah', ['as' => 'user.user.create', 'uses' => 'User\UserController@create']);
            Route::get('/kemaskini/{id?}', ['as' => 'user.user.edit', 'uses' => 'User\UserController@edit']);
            Route::post('/tambah', ['as' => 'user.user.store', 'uses' => 'User\UserController@store']);
            Route::get('/padam/{id}', ['as' => 'user.user.destroy', 'uses' => 'User\UserController@destroy']);
            Route::post('/kemaskini/{id?}', ['as' => 'user.user.update', 'uses' => 'User\UserController@update']);
            Route::get('/dataUser', ['as' => 'user.user.data', 'uses' => 'User\UserController@show']);
            Route::get('/status/{id?}', ['as' => 'user.user.status', 'uses' => 'User\UserController@status']);
            Route::get('/statuss/{id?}', ['as' => 'user.user.statuss', 'uses' => 'User\UserController@statuss']);
        });


	Route::get('/home', 'HomeController@index')->name('home');

	Auth::routes();

});


