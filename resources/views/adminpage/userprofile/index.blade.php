@extends('vadmin.tampilan')

@section('content')

	<!-- Pict CSS -->
    <link href="{{ asset('admin/css/dialpict.css') }}" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="profile large">
		    <div class="cover"><img src="https://source.unsplash.com/random/700x300"/>
		      <div class="layer">
		        <div class="loader"></div>
		      </div><a class="image-wrapper" href="#">
		        <form id="coverForm" action="#">
		          <input class="hidden-input" id="changeCover" type="file"/>
		          <label class="edit glyphicon glyphicon-pencil" for="changeCover" title="Change cover"></label>
		        </form></a>
		    </div>
		    <div class="user-info">
		      <div class="profile-pic"><img src="{{ asset('admin/images/user.png') }}"/>
		        <div class="layer">
		          <div class="loader"></div>
		        </div><a class="image-wrapper" href="#">
		          <form id="upload_form" role="form" action="" enctype="multipart/form-data" method="POST">
		          	{{ csrf_field() }}
		            <input class="hidden-input" id="changePicture" type="file" name="picture" />
		            <label class="edit glyphicon glyphicon-pencil" for="changePicture" type="file" title="Change picture"></label>
		          </form></a>
		      </div>
		      <div class="username">
		        <div class="name"><span class="verified"></span>Wakudiallah</div>
		        <div class="about">wakudiallah@netxpert.com</div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>


	<!-- Pict JS -->
    <!-- <script src="{{ asset('admin/js/dialpict.js') }}"></script>-->
    <script>
		$(document).ready(function (e) {
	    $('#changePicture').on('submit',(function(e) {
	        e.preventDefault();
	        var data = new FormData(this);

	        $.ajax({
	            type: 'post',
	            url: '{!! route('userprofile.profile.insert') !!}',
	            processData: false,
	            contentType: false,
	            data: data,
	            success: function(result){
	                console.log(result);
	            },
	            error: function(error){
	                console.log("error");
	            }
	        });
	    }));
	});
	</script>
@endsection
