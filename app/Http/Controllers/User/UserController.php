<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\UserGroup_param;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminpage.user.index');
    }

    
    public function create()
    {
        $usergroup = UserGroup_param::select('id','nama_workgroup')->get();
        return view('adminpage.user.create', compact('usergroup'));
    }

    public function store(Request $request)
    {
        $data = new User;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = $request->password;
        $data->role = $request->role;
        $data->activation_code  = $request->activation_code;
        $data->save();
        
        return redirect()->route('user.user.index');
    }

    public function show()
    {
        $userdata = User::select(['id','name','email','role', 'activation_code']);
            return Datatables::of($userdata)
             ->editColumn('is_active', function ($inquiry) {
                if ($inquiry->activation_code == 1) return 
                     'Aktif';
                else return 'Tidak Aktif';
            })
            ->addColumn('action', function ($userdata) {
                    if ($userdata->activation_code == 0) return
                    '<a href="'.route("user.user.status", ["id"=>$userdata ]).'" class="btn btn-xs btn-primary" ><i class="glyphicon glyphicon-ok"></i> Aktifkan </a>';
                   else return
                   '<a href="'. route('user.user.statuss', $userdata->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Non Aktifkan</a> 
                   ' ;

            })->make(true);
    }

    public function status(Request $request, $id)
    {
        //app("User")->find($request->id)->update(['activation_code' => '1']);
        User::query()->update(['activation_code' => 1]);
        return redirect()->route('user.user.index');
    }

    public function statuss(Request $request, $id)
    {
        //app("User")->find($request->id)->update(['activation_code' => '1']);
        User::query()->update(['activation_code' => 0]);
        return redirect()->route('user.user.index');
    }

    
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
