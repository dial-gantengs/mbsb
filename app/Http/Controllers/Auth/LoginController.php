<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\Model\UserGroup_param;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    

    use AuthenticatesUsers;
    //use EntrustUserTrait;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function Loginrule(Request $request)
    {
        if (Auth::attempt([
            'email'     => $request->email,
            'password'  => $request->password
        ])){
            if(Auth::user()->activation_code == 0){

                Auth::logout();
                return redirect('login')->withErrors(['Your account is not approved']);

            }elseif(Auth::user()->activation_code == 1 && Auth::user()->role == 1){

                return redirect('/adminnew');

            }elseif(Auth::user()->activation_code == 1 && Auth::user()->role == 2){

                return redirect('/adminnew1');

            }else{
                Auth::logout();
                return redirect('login')->withErrors(['Your account is not in record']);
            }
            
        }else{
            return redirect('/login')->withErrors(['Incorrect Email or Password']);
        }
    }
    
}
