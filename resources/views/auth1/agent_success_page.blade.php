<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Apply to be an Agent";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
        <div id="main" role="main">
<br><br><br><br>
            <!-- MAIN CONTENT -->
            <div id="content" class="container">
          @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
          <script>
                        function pesan() {
                            bootbox.alert("<b>{{Session::get('error')}}</b>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>

        @elseif (Session::has('success'))
              <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
           <script>
                        function pesan() {
                            bootbox.alert("<center><b>{{Session::get('success')}}</b></center>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>
            
      @endif
           
          
                <div class="row">
                    <div class="col-xs-11 col-sm-12 col-md-7 col-lg-7">

                 
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <img src="{{url('/asset/img/ezlestari_agent.png')}}" width="650"/>
		 <br>
	   
                           <div class="visible-lg">  <br> 
                            <b><u> Menjadi Agent Kami </u></b>
                           <p align="justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <p align="justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                           </p>

                           <a class="btn btn-default btn-sm" href="#"><i class="fa fa-download"></i> &nbsp; Muat Turun Terma & Syarat</a>
                        </div>
  
                        </div>
                        
                 
                    </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                         <div class="well no-padding">
                


   {!! Form::open(['url' => 'agent_registration','class' => 'smart-form client-form', 'id' => 'agent-form-register' ]) !!}
   
                
                        <header>
									<p class="txt-color-white"><b>    Apply to be an agent  </b> </p>
						</header>

                        <fieldset>
                        <input type="hidden" name="random_code" value="{{$random_code}}"/>

                        @if (count($errors) > 0)
     <div class="alert adjusted alert-warning fade in">
                                            <button class="close" data-dismiss="alert">
                                                ×
                                            </button>
            @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
            @endforeach
         </div>
@endif
 

    {!! csrf_field() !!}
                         
                            
                                          
                            <section>
                                <label class="label">Nama Penuh (seperti dalam KP) /<i> Full Name (as in IC)</i>:</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" name="fullname" id="fullname">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Nama Penuh (seperti dalam KP) / Full Name (as in IC)</b></label>
                            </section>

                            <section>
                                <label class="label">No Kad Pengenalan /<i> IC Number</i>:</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" minlength="12" maxlength="12" name="icnumber" id="icnumber">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> No Kad Pengenalan / IC Number</b></label>
                            </section>

                      

                            <section>
                                <label class="label">No Untuk Dihubungi /<i> Contact No</i>:</label>
                                <label class="input">
                                 <i class="icon-append fa fa-phone"></i>
                                    <input autocomplete="new-phone" placeholder="Kediaman / House No" type="text" name="houseno" id="houseno">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Kediaman / House No</b>
                                </label><br>

                                <label class="input">
                                 <i class="icon-append fa fa-phone"></i>
                                    <input autocomplete="new-phone" placeholder="Tel Pejabat / Office No" type="text" name="officeno" id="officeno">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Tel Pejabat / Office No</b>
                                </label><br>

                                <label class="input">
                                 <i class="icon-append fa fa-mobile"></i>
                                    <input autocomplete="new-phone" placeholder="Tel Bimbit / Handphone" type="text" name="phoneno" id="phoneno">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Tel Bimbit / Handphone</b>
                                </label>
                            </section>
                             <section>
                                <label class="label">E-mel /<i> E-mail</i>:</label>

                                 <label class="input">
                                 <i class="icon-append fa fa-envelope"></i>
                                    <input placeholder="" autocomplete="new-email" type="email" name="email" id="email">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> E-mel / E-mail</b>
                                </label>

                            </section>
                            <section>
                              
                                   <label class="label">Salinan Kad Pengenalan /<i> Copy IC</i>:</label>
                                  <label class="input">
                                 
                                      <input id="fileupload1"  @if(empty($document6->name)) required @endif    class="btn btn-default" type="file" name="file1" >
                                      <input type="hidden" name="document1"   id="documentx1"  value="Salinan Kad Pengenalan">
                                      &nbsp; <span id="document1"> </span> 
                                      <input type='hidden' value='@if(!empty($document1->name)){{$document1->upload}}@endif' id='a1' name='a1'/>
                                      @if(!empty($document1->name))
                                        <span id="document6a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document1->upload}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                      @endif
                    
                        
                                
                                                
                                    </label>
                          </section>
                            <section>
                            
                                          <label class="label"> Front Page Buku Bank :</label>
                                  <label class="input">
                                 
                                      <input id="fileupload2"  @if(empty($document2->name)) required @endif    class="btn btn-default" type="file" name="file2" >
                                      <input type="hidden" name="document2"   id="documentx2"  value="Front Page Buku Bank">
                                      &nbsp; <span id="document2"> </span> 
                                      <input type='hidden' value='@if(!empty($document2->name)){{$document2->upload}}@endif' id='a2' name='a2'/>
                                      @if(!empty($document2->name))
                                        <span id="document2a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document2->upload}}"> {{$document2->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                      @endif
                    
                        
                                
                                                
                                    </label>
                          </section>



                       

                            
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                Next
                            </button>
                        </footer>
                  {!! Form::close() !!}  

                </div>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">

                    
                        
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <div class="hidden-lg">    <b><u> Menjadi Agent Kami </u></b>
                            </div>
                  <br>
                               
                      

            </div>

        </div>
            </div> </div>

        </div>
             
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body custom-scroll terms-body">
                        
 <div id="left">



            <h1>SMARTADMIN TERMS & CONDITIONS TEMPLATE</h1>



            <h2>Introduction</h2>

            <p>bla blas</p>



            </div>
            
            <br><br>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="button" class="btn btn-primary" id="i-agree">
                            <i class="fa fa-check"></i> I Agree
                        </button>
                        
                        <button type="button" class="btn btn-danger pull-left" id="print">
                            <i class="fa fa-print"></i> Print
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

        $(document).ready(function() {
                
                    $("#smart-form-register2").hide();
                $("#agent-form-register").validate({

                    // Rules for form validation
                    rules : {
                        fullname: {
                            required : true
                        },
                        icnumber : {
                            required : true
                        },
                        
                        phoneno: {
                            required: true
                        },
                        houseno: {
                            required: true
                        },
                        
                        email: {
                            required: true
                        },
                        residential: {
                            required: true
                        },
                        fileupload1: {
                            required: true
                        },
                        fileupload2: {
                            required: true
                        }
                    },

                    // Messages for form validation
                    messages : {

                        fullname: {
                            required : 'Please enter your full name'
                        },
                        
                        icnumber: {
                            required: 'Please enter your ic number'
                        },
                        residential: {
                            required: 'Please enter your residential address'
                        },
                        houseno: {
                            required: 'Please enter your house phone no.'
                        },
                        phoneno: {
                            required: 'Please enter your handphone no.'
                        },
                        email: {
                            required: 'Please enter your email address'
                        },
                        fileupload1: {
                            required: 'Please upload your copy IC'
                        },
                        fileupload2: {
                            required: 'Please enter your front page bank book'
                        }
                    }


                });

      });
    </script>


<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'agent_registration/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/agent_temp/{{str_replace('/', '',$random_code)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
      $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>


<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>
<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
        

