<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Apply to be an Agent";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
        <div id="main" role="main">
<br><br><br><br>
            <!-- MAIN CONTENT -->
            <div id="content" class="container">
          @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
          <script>
                        function pesan() {
                            bootbox.alert("<b>{{Session::get('error')}}</b>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>

        @elseif (Session::has('success'))
              <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
           <script>
                        function pesan() {
                            bootbox.alert("<center><b>{{Session::get('success')}}</b></center>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>
            
      @endif
           
          
                <div class="row">
                    <div class="col-xs-11 col-sm-12 col-md-7 col-lg-7">

                 
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <img src="{{url('/asset/img/ezlestari_agent.png')}}" width="650"/>
		 <br>
	   
                           <div class="visible-lg">  <br> 
                            <b><u> Menjadi Ejen Kami </u></b>
                           <p align="justify">
                            Cara mudah untuk menjana pendapatan pada bila-bila masa dan di mana sahaja.<br> Daftar sekarang sebagai ejen pembiayaan peribadi online dan nikmati faedah berikut :</p>
                            <p align="justify">
                            <ol type="1">
                              <li> Akses percuma pada sistem yang disediakan, hanya lengkapkan maklumat pelanggan dan hantar secara online.</li>
                              <li> Komisen akan di bayar sebanyak 0.5% daripada jumlah pembiayaan yang dikeluarkan (disbursed amount)</li>
                              <li> Tiada yuran akan dikenakan kepada ejen yang dilantik</li>
                            </ol>
                           </p>

                    
                        </div>
  
                        </div>
                        
                 
                    </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                         <div class="well no-padding">
                


   {!! Form::open(['url' => 'agent_registration','class' => 'smart-form client-form', 'id' => 'agent-form-register' ]) !!}
   
                
                        <header>
									<p class="txt-color-white"><b>    Mohon Sebagai Ejen Sekarang  </b> </p>
						</header>

                        <fieldset>
                        <input type="hidden" name="random_code" value="{{$random_code}}"/>

                        @if (count($errors) > 0)
     <div class="alert adjusted alert-warning fade in">
                                            <button class="close" data-dismiss="alert">
                                                ×
                                            </button>
            @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
            @endforeach
         </div>
@endif
 

    {!! csrf_field() !!}
                         
                            
                                          
                            <section>
                                <label class="label">Nama Penuh (seperti dalam KP) /<i> Full Name (as in IC)</i>:</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" name="fullname"  onkeyup="this.value = this.value.toUpperCase()" id="fullname">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Nama Penuh (seperti dalam KP) / Full Name (as in IC)</b></label>
                            </section>

                            <section>
                                <label class="label">No Kad Pengenalan /<i> IC Number</i>:</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" onkeypress="return isNumberKey(event)"  minlength="12" maxlength="12" name="icnumber" id="icnumber">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> No Kad Pengenalan / IC Number</b></label>
                            </section>

                      

                            <section>
                                <label class="label">No Untuk Dihubungi /<i> Contact No</i>:</label>

                                <label class="input">
                                 <i class="icon-append fa fa-mobile"></i>
                                    <input autocomplete="new-phone" placeholder="Tel Bimbit / Handphone" type="text" minlength="7" maxlength="12"  onkeypress="return isNumberKey(event)" name="phoneno" id="phoneno">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Tel Bimbit / Handphone</b>
                                </label>
                            </section>
                             <section>
                                <label class="label">E-mel /<i> E-mail</i>:</label>

                                 <label class="input">
                                 <i class="icon-append fa fa-envelope"></i>
                                    <input placeholder="" autocomplete="new-email" type="email" name="email" id="email">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> E-mel / E-mail</b>
                                </label>

                            </section>

                            
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                Apply
                            </button>
                        </footer>
                  {!! Form::close() !!}  

                </div>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">

                    
                        
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <div class="hidden-lg">    <b><u> Menjadi Agent Kami </u></b>
                            </div>
                  <br>
                               
                      

            </div>

        </div>
            </div> </div>

        </div>
             
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body custom-scroll terms-body">
                        
 <div id="left">



            <h1>SMARTADMIN TERMS & CONDITIONS TEMPLATE</h1>



            <h2>Introduction</h2>

            <p>bla blas</p>



            </div>
            
            <br><br>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="button" class="btn btn-primary" id="i-agree">
                            <i class="fa fa-check"></i> I Agree
                        </button>
                        
                        <button type="button" class="btn btn-danger pull-left" id="print">
                            <i class="fa fa-print"></i> Print
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

        $(document).ready(function() {
                
                    $("#smart-form-register2").hide();
                $("#agent-form-register").validate({

                    // Rules for form validation
                    rules : {
                        fullname: {
                            required : true
                        },
                        icnumber : {
                            required : true
                        },
                        
                        phoneno: {
                            required: true
                        },
                        houseno: {
                            required: true
                        },
                        
                        email: {
                            required: true
                        },
                        residential: {
                            required: true
                        }
                    },

                    // Messages for form validation
                    messages : {

                        fullname: {
                            required : 'Please enter your full name'
                        },
                        
                        icnumber: {
                            required: 'Please enter your ic number'
                        },
                        residential: {
                            required: 'Please enter your residential address'
                        },
                        houseno: {
                            required: 'Please enter your house phone no.'
                        },
                        phoneno: {
                            required: 'Please enter your handphone no.'
                        },
                        email: {
                            required: 'Please enter your email address'
                        }
                    }


                });

      });
    </script>


<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'agent_registration/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/agent_temp/{{str_replace('/', '',$random_code)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
             $("#a{{$x}}").val(data.file);
            $("#a{{$x}}").val(data.file);
      $("#a{{$x}}").val(data.file);

             document.getElementById("fileupload{{$x}}").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>
<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
        

