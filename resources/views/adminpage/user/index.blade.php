@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix"><!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        <li class="active"><i class="material-icons">person_add</i> User</li>
                    </ol>
                </div>
            </div><!-- End of breadcrumber -->
            
            <div class="row clearfix demo-button-sizes">
                <div class="col-md-10"></div>
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
                    <a href="{!! route('user.user.create') !!}" type="submit" class="btn bg-red btn-block btn-lg waves-effect" style="cursor:pointer;">
                      Tambah
                    </a>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>Data Users</h2>
                        </div>
                        <div class="body">
                            <table class="table" id="data-datatable">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Name</td>
                                        <td>Email</td> 
                                        <td>Status</td>
                                        <td>Tindakan</td>              
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


 <!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- DataTables -->
<!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->

<script type = "text/javascript">
    $(function(){
        $("#data-datatable").DataTable({
            "lengthChange": true,
            processing    : true,
            serverSide    : true,
            ajax          : '{!! route('user.user.data') !!}',
            columns       : [
            { data        : 'id', name: 'id' },
            { data        : 'name', name: 'name' },
            { data        : 'email', name: 'email' },
            { data        : 'is_active', name: 'is_active' },
            {data         : 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
