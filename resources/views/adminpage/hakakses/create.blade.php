@extends('vadmin.tampilan')

@section('content')

    <section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix"> <!-- Breadcrumber -->
                <div class="col-md-6">
                    <ol class="breadcrumb breadcrumb-col-pink">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i> User</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">touch_app</i> Hak Akses</a></li>
                        @if(Request::segment(3) === 'tambah')
                        <li class="active"><i class="material-icons">create</i> Create</li>
                        @elseif(Request::segment(3) == 'kemaskini')
                        <li class="active"><i class="material-icons">edit</i> Kemaskini</li>
                        @endif
                    </ol>
                </div>
            </div> <!-- End of breadcrumber -->
            
            <div class="row clearfix">
    			<div class="col-md-12">
    				<div class="card">
    					<div class="header bg-red">
    						<h2>Tambah</h2>
    					</div>

    					<div class="body">
    						<div class="row clearfix">
    							<form method="POST" action="@yield('url',route("user.hakakses.create") )">
    								{{ csrf_field()}}
    	
	                                <div class="col-sm-12">
	                                	<h5>Kod Work Group :</h5>
	                                    <div class="form-group form-float">
	                                        <div class="form-line">
	                                        	<input type="text" class="form-control" name="kod_workgroup" value="@yield('kod_workgroup')" required autofocus>
	                                            <label class="form-label">Kod Work Group</label>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-sm-12">
	                                	<h5>Nama Work Group :</h5>
	                                    <div class="form-group form-float">
	                                        <div class="form-line">
	                                        	<input type="text" class="form-control" name="nama_workgroup" value="@yield('nama_workgroup')" required autofocus>
	                                            <label class="form-label">Kod Work Group</label>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-sm-12">
	                                	<h5>Keterangan :</h5>
	                                    <div class="form-group form-float">
	                                        <div class="form-line">
	                                            <input type="text" class="form-control" name="keterangan" value="@yield('keterangan')" required autofocus/>
	                                            <label class="form-label">Keterangan</label>
	                                        </div>
	                                    </div>
	                                </div>

                                	<div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 col-md-offset-10">
					                    <button type="submit" class="btn bg-red btn-block btn-lg waves-effect">
		                                    Simpan
		                                </button>
					                </div>
                                </form>
                            </div>
    					</div>
    				</div>
    			</div>
    			
    		</div>
        </div>
    </section>
@endsection