<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class UserGroup_param extends Model
{
    protected $table = 'UserGroup_param';
    protected $fillable = [
       'kod_workgroup','nama_workgroup','keterangan'
    ];

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates = ['deleted_at'];
    public $timestamps = true;

}
