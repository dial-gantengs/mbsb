<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserGroup_paramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		foreach(range(1, 10) as $index) {
			app('UserGroup_param')->create([
				'kod_workgroup'   		=> $faker->randomLetter(10).$index,
				'nama_workgroup'        => $faker->randomLetter(30),
                'keterangan'            => $faker->Text(),
			]);
		}
    }
}
