@extends('layouts.main')

@section('content')

  <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/kl.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
    <div class="overlay"></div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md">
          <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Lorem Ipsum is simply dummy text of</h2> 
          <p class="lead mb-5 probootstrap-animate">
          <!-- </p>
            <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
          </p> -->
        </div> 
        <div class="col-md probootstrap-animate">
          <form action="{{url(action('Auth\LoginController@loginrule'))}}" method="POST" class="probootstrap-form border border-danger">
            {{ csrf_field() }}

            <div class="form-group">
              <div class="col-md">
                   <div class="panel panel-default">

                      <div class="panel-body">
                          <div class="form-group">
                              <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                              <div class="probootstrap-date-wrap">
                                  <span class="icon ion-card"></span>
                                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="probootstrap-date-departure" class="col-md-12 control-label">Password</label>

                              <div class="probootstrap-date-wrap">
                                  <span class="icon ion-key"></span>
                                  <input id="password" type="password" class="form-control" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                  <div class="checkbox">
                                      <label>
                                          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                      </label>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="row">
                                <div class="col-md"></div>
                                <div class="col-md">
                                  <button type="submit" class="btn btn-danger btn-block" style="cursor:pointer;">
                                      Login
                                  </button>
                                </div>
                              </div>
                              <div class="col-md-12 link-text">
                                  <a href="{{ route('password.request') }}" onmouseover="this.style.color='red';" onmouseout="this.style.color='black';">
                                      Lupa kata laluan?
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div> 
                </div>   
            </div>
          </form>
        </div>



      </div>
    </div>
  </section>

@endsection
