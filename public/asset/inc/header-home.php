		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">

				<span id="logo"> <a href="<?php print url('/') ?>"> <img src="<?php echo ASSETS_URL; ?>/img/logo.png" alt="SmartAdmin" width="300"> </a></span>

				<!-- END AJAX-DROPDOWN -->
			</div>


<?php 	if(empty($user->name)) { ?>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-danger">Contact</a> </span>


			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/faq" class="btn hidden-xs btn-danger">FAQ</a> </span>

			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/login" class="btn hidden-xs btn-danger">Sign In</a> </span>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-danger">Home</a> </span>
			<!-- Single button -->
			<span id="extr-page-header-space"> 
				<div class="dropdown visible-xs center">
				  <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
					 &nbsp; &nbsp;Menu &nbsp; &nbsp; &nbsp;<span class="caret"></span>
				  </a>
				  <ul class="dropdown-menu">
					<li><a href="<?php print url('/') ?>">Home</a></li>
					<li><a href="<?php print url('/') ?>/login">Sign in</a></li>
					<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>
					<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
				</ul>
				</div>
			</span>
			<span id="extr-page-header-space"> &nbsp;</span><span id="extr-page-header-space"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <?php 
		} 
		else {?>
            <span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-danger">Contact</a> </span>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/faq" class="btn hidden-xs btn-danger">FAQ</a> </span>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/logout" class="btn hidden-xs btn-danger">Sign Out</a> </span>
			<?php if($user->role=='0' || $user->role=='7') { ?>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/home" class="btn hidden-xs btn-danger">User Area</a> </span>

			<?php }
			elseif($user->role=='6') { ?>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/admin" class="btn hidden-xs btn-danger">Agent Area</a> </span>

			<?php } 
			else { ?>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/admin" class="btn hidden-xs btn-danger">Admin Area</a> </span>
			<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-danger">Home</a> </span>
			
			<?php } ?>
			
			<!-- Single button -->
			<span id="extr-page-header-space"> 
				<div class="dropdown visible-xs center">
				  <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
					 &nbsp; &nbsp;Menu &nbsp; &nbsp; &nbsp;<span class="caret"></span>
				  </a>
				  <ul class="dropdown-menu">
					<?php if($user->role=='0' || $user->role=='7'){ ?>
					<li><a href="<?php print url('/') ?>/home">User Area</a></li>
					<?php }
					elseif($user->role=='6') { ?>
					<li><a href="<?php print url('/') ?>/admin">Agent Area</a></li>
					<?php }
					else { ?>
					<li><a href="<?php print url('/') ?>/admin">Admin Area</a></li>
					<li><a href="<?php print url('/') ?>">Home</a></li>
					<?php } ?>
					<li><a href="<?php print url('/') ?>/logout">Sign Out</a></li>
					<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>
					<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
				</ul>
				</div>
			</span>
			<span id="extr-page-header-space"> &nbsp;</span><span id="extr-page-header-space"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<?php 
		} ?>


		</header>
