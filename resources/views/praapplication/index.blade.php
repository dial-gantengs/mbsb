<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Pinjaman Peribadi i-Lestari ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
    <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     @if (Session::has('message'))
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif             
            @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
           
          
				<div class="row">
					<div class="col-xs-11 col-sm-12 col-md-7 col-lg-7">


        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">

<script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
</script>
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_2_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_2_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_2_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_2_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_2_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
     ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        });
    </script>
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_3_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_3_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_3_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_3_slider = new $JssorSlider$("jssor_3", jssor_3_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_3_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_3_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
</script>

<style>
    
    /* jssor slider bullet navigator skin 05 css */
    /*
    .jssorb05 div           (normal)
    .jssorb05 div:hover     (normal mouseover)
    .jssorb05 .av           (active)
    .jssorb05 .av:hover     (active mouseover)
    .jssorb05 .dn           (mousedown)
    */
    .jssorb05 {
        position: absolute;
    }
    .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
        position: absolute;
        /* size of bullet elment */
        width: 16px;
        height: 16px;
        background: url('{{url('/')}}/img/b05.png') no-repeat;
        overflow: hidden;
        cursor: pointer;
    }
    .jssorb05 div { background-position: -7px -7px; }
    .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
    .jssorb05 .av { background-position: -67px -7px; }
    .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

    /* jssor slider arrow navigator skin 22 css */
    /*
    .jssora22l                  (normal)
    .jssora22r                  (normal)
    .jssora22l:hover            (normal mouseover)
    .jssora22r:hover            (normal mouseover)
    .jssora22l.jssora22ldn      (mousedown)
    .jssora22r.jssora22rdn      (mousedown)
    */
    .jssora22l, .jssora22r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 58px;
        cursor: pointer;
        background: url('{{url('/')}}/img/a22.png') center center no-repeat;
        overflow: hidden;
    }
    .jssora22l { background-position: -10px -31px; }
    .jssora22r { background-position: -70px -31px; }
    .jssora22l:hover { background-position: -130px -31px; }
    .jssora22r:hover { background-position: -190px -31px; }
    .jssora22l.jssora22ldn { background-position: -250px -31px; }
    .jssora22r.jssora22rdn { background-position: -310px -31px; }
</style>

  <div align='center'><a href='{{ url('/') }}/applynow'><img data-u="image" src="{{ url('/') }}/img/mohon_sekarang.png" /></a>

 <span class="hidden-xs">&nbsp; &nbsp;<a href='{{ url('/') }}/agent_registration'><img data-u="image" src="{{ url('/') }}/img/daftar_ejen.png" /></a></span>

 
 <span class="visible-xs"><br><a href='{{ url('/') }}/agent_registration'><img data-u="image" src="{{ url('/') }}/img/daftar_ejen.png" /></a></span>


 </div><br>

    <div class="well hidden-xs" id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
           
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/blue.jpg">
          <img data-u="image" src="{{ url('/') }}/img/blue.jpg" />
        </a>
            </div>
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/purple.jpg">
          <img data-u="image" src="{{ url('/') }}/img/purple.jpg" />
        </a>
            </div>
            
            
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-05.jpg"> 
         <img data-u="image" src="{{ url('/') }}/img/WEB-05.jpg" />
        </a>
            </div>
              
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-07.jpg"> 
          <img data-u="image" src="{{ url('/') }}/img/WEB-07.jpg" />
        </a>
            </div>        
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>
    
	    <div class="well visible-xs" id="jssor_3" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
           
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/blue.jpg">
          <img data-u="image" src="{{ url('/') }}/img/blue.jpg" />
        </a>
            </div>
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/purple.jpg">
          <img data-u="image" src="{{ url('/') }}/img/purple.jpg" />
        </a>
            </div>
            
            
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-05.jpg"> 
         <img data-u="image" src="{{ url('/') }}/img/WEB-05.jpg" />
        </a>
            </div>
              
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-07.jpg"> 
          <img data-u="image" src="{{ url('/') }}/img/WEB-07.jpg" />
        </a>
            </div>		  
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo1.jpg"><img data-u="image" src="{{ url('/') }}/img/promo1.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo2.jpg"><img data-u="image" src="{{ url('/') }}/img/promo2.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo3.jpg"><img data-u="image" src="{{ url('/') }}/img/promo3.jpg" /></a>
            </div>

             
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
      </div>

		 <br>

	    <div id="jssor_2" class="well hidden-xs"  style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">

        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
           
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo1.jpg"><img data-u="image" src="{{ url('/') }}/img/promo1.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo2.jpg"><img data-u="image" src="{{ url('/') }}/img/promo2.jpg" /></a>
            </div>
            <div data-p="225.00" style="display: none;">
                  <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/promo3.jpg"><img data-u="image" src="{{ url('/') }}/img/promo3.jpg" /></a>
            </div>

      

        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>
    <br>
            </div>
                        
                        
          </div>

          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="well no-padding">
              <form action="praapplication" class="smart-form client-form" id="smart-form-register">
                <header>
                  <p class="txt-color-white"><b>  Mohon Sekarang  </b> </p>
                </header>

                <fieldset>
                  <div class="row">
                     <div class="col-xs-6 col-12">
                        <section class="col col-12">
                          <label class="label"> <b> Nama Penuh </b></label>
                            <label class="input">
                                <i class="icon-append fa fa-user"></i>
                                <input type="text" id="FullName" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif >
                                <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                            </label>
                        </section>
                        </div>
                        <div class="col-xs-6 col-12">
                          <section class="col col-12">
                            <label class="label"> <b> No Kad Pengenalan </b></label>
                            <label class="input">
                                <i class="icon-append fa fa-user"></i>
                                <input type="text" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif >
                                <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                            </label>
                          </section>
                        </div>
                    </div>
                                        
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12">
                              <label class="label"> <b> Nombor Telefon Bimbit</b> </label>
                              <label class="input">
                                <i class="icon-append fa fa-mobile-phone"></i>
                                <input type="text" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                              </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12">
                                            <label class="label"> <b> Jenis Pekerjaan </b></label>
                                                <label class="select">
                                                    <select name="Employment" id="Employment" class="StatusPekerjaan form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employment')) 
                                                    <option  value="{{ Session::get('employment') }}">{{ Session::get('employment2') }}</option>
                                                
                                                    @endif
                                                       
                                                         
                            <option value="">employmentname</option>
                                                    </select> <i></i>

                                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                                </label>
                                            </section>
                                        </div>
                                      
                                        </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-xs-6 col-12">
                                            <section class="col col-12">
                                              <label class="label"> <b> Majikan</b></label>
                                              <label class="select" id="majikan">
                                              <select name="Employer" id="Employer" class="form-control " onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employer')) 
                                                    <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>  
                                                    @endif
                                              </select> <i></i>
                                              <input type="hidden" name="Employer2" id="Employer2" value="" />
                                              </label>
                                               <label id="majikan2" class="input @if (Session::has('basicsalary')) state-error @endif ">
                                              <i class="icon-append fa fa-briefcase"></i>
                                              <input type="text" id="majikantext" name="majikan"  placeholder="Majikan" maxlength="70"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif >
                                              <b class="tooltip tooltip-bottom-right"> Majikan</b>
                                            </label>
                                          </section>
                                        </div>
                                       
                                        <div class="col-xs-6 col-12" id="statuspekerjaan" style="display: block;">
                                          <section class="col col-12 col-lg-12">
                                            <label class="label"> <b> Status Pekerjaan</b></label>
                                            <label class="select">
                                                <select name="StatusPekerjaan" placeholder="Status Pekerjaan" id="StatusPekerjaan" required class="StatusPekerjaan form-control">
                                                <option disabled selected>Status Pekerjaan</option>
                                                    <option value='1'>Tetap</option>
                                                    <option value='0'>Kontrak</option>
                                                </select>  <i></i>
                                            </label>
                                            <div id="toro"></div>
                                          </section>
                                          </div>

                                        <div class="col-xs-6 col-12" id="masakerja" style="display:none;">
                                          <section class="col col-12 col-lg-12 col-md-12">
                                              <label class="label"> <b> Tempoh Berkhidmat</b></label>
                                              <label class="select">
                                              <select name="MasaBerkhidmat" placeholder="MasaBerkhidmat" id="MasaBerkhidmat" required class="StatusPekerjaan form-control">
                                                  <option disabled selected>Tempoh Berkhidmat</option>
                                                  <option value='0'>Kurang dari pada 3 Tahun</option>
                                                  <option value='1'>Lebih dari pada 3 Tahun</option>        
                                              </select>  <i></i>
                                              </label>
                                            </section>
                                        </div>
                                      </div>

                                      <div class="row">
                                          <div class="col-xs-12 col-12" id="kemas" style="display:none;">
                                              <section class="col col-12">
                                                <label class="label col-xs-12"> <b>Adakah anda kakitangan KEMAS/Jab Perpaduan yang telah berkidmat lebih dari 2 tahun ?</b></label>
                                                <label id="fieldkemas" class=" col-xs-6">
                                                <input type='radio' required name="kemas" value="1">Ya
                                                <input type='radio' required name="kemas" value="0">Tidak
                                                </label>
                                                <span id="messages"></span>
                                              </section>
                                          </div>
                                      </div>
                                      </fieldset>
                                      
                                      <fieldset>
                                         <div class="row">
                                          <div class="col-xs-6 col-12">
                                            <section class="col col-12">
                                                <label class="label"> <b> Gaji Asas (RM) </b>  </label>
                                                <label class="input @if (Session::has('basicsalary2')) state-error @endif ">
                                                    <i class="icon-append fa fa-credit-card"></i>
                                                    <input type="text" name="BasicSalary" id="BasicSalary" placeholder="Basic Salary (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                                                    <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>
                                                </label>
                                            </section>
                                        </div>
                                        <div class="col-xs-6 col-12">
                                          <section class="col col-12">
                                              <label class="label"> <b> Elaun (RM) </b></label>
                                              <label class="input">
                                              <i class="icon-append fa fa-credit-card"></i>
                                              <input type="text" name="Allowance" id="Allowance" placeholder="Allowance (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >
                                              <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>
                                              </label>
                                          </section>
                                        </div>
                                        </div>

                                    <div class="row">
                                      <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                                    <label class="label"><b> Jumlah Potongan Bulanan Semasa (RM) </b> </label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Deduction" id="Deduction" placeholder="Existing Total Deduction (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>
                                                    </label>
                                                </section>
                                                </div>
                                              </div>
                                                
                                            
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-lg-12 col-md-12">
                                            <label class="label"> <b> Pakej</b><div class="visible-xs"><br></div> </label>
                                                <label class="select">
                                                    <select name="Package" id="Package" class="form-control">
                                                        
                                                        
                            <option value="">package name</option>
                              
                                                    </select> <i></i>
                                                </label>
                                           </section>
                                            </div>
                                        <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> Jumlah Pembiayaan(RM) </b></label>
                                            <label class="input @if (Session::has('loanAmount2')) state-error @endif ">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="LoanAmount" id="LoanAmount" placeholder="Loan Amount (RM)" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif>
                                                 
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan (RM)</b>
                                            </label>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </section>
                                        </div>
                                        </div>

                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-danger">
                                     <b>    Kira Kelayakan  </b>
                                    </button>
                                    <div id="response"></div>
                                </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>                          
          </div>
      </div>

    </div>
    <div class="visible-lg"><font size="2" >--Advertisement--</font><br>

      <a href="http://www.car2cash.com.my" target="_blank">
          <img align="center" src="{{url('/img/c2c_1130x125.png')}}">
      </a>

    </div>
    <div align="center" class="hidden-lg"><font size="2" >--Advertisement--</font>

         <a  href="http://www.car2cash.com.my" target="_blank">
            <img src="{{url('/img/c2c_300x50.png')}}">
          </a>

    </div>
          

         <br><br><br>
    <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">

          
            
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          

                        
                            <b><u> Konsep Pembiayaan i-Lestari </u></b>
                            <ul>
                            <li>
    Pembiayaan <b> i-Lestari </b>  adalah berkonsepkan  <b> Tawarruq </b> </li>

                            </ul>

                            <br>
                            <b><u>Kategori Pelanggan </u></b> 
                            <br>
                             1. Persendirian-i Lestari Awam 
                            <ul>
                            <li> Sektor Kerajaan dan Badan Berkanun <b> berstatus tetap </b>  dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan <b>  status kontrak bersambung </b> tidak kurang 2 tahun berkhidmat hanya kepada <b> Kakitangan KEMAS dan Jabatan Perpaduan sahaja. </b> </li>
                        <li>Anak Syarikat Kerajaan / GLC </li>
                            </ul>
                              2.    i-Lestari Swasta 
                             <ul>
                             <li>
                             Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)
                             </li>
                             </ul>

                                  <b><u> Jumlah dan Had Pembiayaan</u></b> <br><br>
<table style="BORDER-COLLAPSE: collapse" border="1" cellspacing="1" width="100%">

<tr>
<td colspan="2" align="center" bgcolor="#e8a74c" width="434">
<span style="LETTER-SPACING: 1pt; FONT-WEIGHT: 700">
<font style="FONT-SIZE: 9pt" face="Arial">Pakej</font>
</span>
</td>
<td align="center" bgcolor="#e8a74c">
<font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt; FONT-WEIGHT: 700" face="Arial">Had Pembiayaan</font>
</td>
</tr>
<tr>
<td colspan="2" align="center" bgcolor="#c74e59" height="19" width="434"><span style="LETTER-SPACING: 1pt"><font style="FONT-SIZE: 9pt" color="#ffffff" face="Arial">Persendirian i-Lestari Awam</font></span><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial"> (Kerajaan / Badan Berkanun / GLC)</font></td>
<td align="center" bgcolor="#c74e59" height="19"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">RM 200,000.00</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Persendirian i-Lestari Swasta Panel Majikan</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Persendirian i-Lestari Swasta Bukan Panel Majikan</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">(Tertakluk Kepada Had Maksima RM 200,000.00)</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : RM3,000.00 - RM 5,000.00</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">TIADA</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">8 Kali Gaji Kasar Sebulan</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : RM5,001.00 - RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : RM5,001.00 - RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">10 Kali Gaji Kasar Sebulan</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="168"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : ≥ RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59" width="262"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Pendapatan : ≥ RM 10,000.00</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">15 Kali Gaji Kasar Sebulan</font></td></tr></table>
  
<br>
                            <b><u>Kadar Keuntungan </u></b> 
                            <br>
                             1. Persendirian-i Lestari Awam 
                            <ul>
                            <li> 1 - 3 tahun = 3.90% setahun   </li>
                        <li>4 - 10 tahun = 4.50% setahun </li>
                            </ul>
                              2.    i-Lestari Swasta Panel Majikan (Kadar Tetap)
                            <ul>
                            <li> Pendapatan RM 3,000.00 - RM 5,000.00 = 7.00% setahun   </li>
                        <li>Pendapatan RM 5,001.00 - RM 10,000.00 = 6.25% setahun  </li>
                        <li>Pendapatan > RM 10,001.00 = 6.00% setahun </li>
                            </ul>

                              3.    i-Lestari Swasta Bukan Panel Majikan (Kadar Tetap)
                            <ul>
                            <li> Pendapatan RM5,001 - RM10,000 = 6.50% Setahun   </li>
                       
                        <li>Pendapatan > RM10,000 = 6.25% Setahun </li>
                            </ul>
                              <b>  (Gaji Kasar = Gaji Pokok + Elaun Tetap)</b>
                              <br>
                              <br>
                              <b><u>Laporan CCRIS</u></b> 
                           
                            <ul>
                            <li> 
    Laporan CCRIS (dari aplikasi 'Credit Bureau') perlu dikemukakan untuk setiap permohonan.    </li>
                     

<table style="BORDER-COLLAPSE: collapse" align="center" border="1" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#e8a74c" width="129"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Tunggakan</font></b></span></td>
<td align="center" bgcolor="#e8a74c" width="226"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Kelulusan</font></b></span></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="129"><span style="LETTER-SPACING: 1pt"><font style="FONT-SIZE: 9pt" color="#ffffff" face="Arial">1 Bulan</font></span></td>
<td align="center" bgcolor="#c74e59" width="226"><span style="LETTER-SPACING: 1pt"><font style="FONT-SIZE: 9pt" color="#ffffff" face="Arial">Kelulusan secara budibicara</font></span></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="129"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">2 Bulan</font></td>
<td align="center" bgcolor="#c74e59" width="226"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Perlu menyatakan asas kelulusan dan mendapat kelulusan tertinggi</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="129"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">3 Bulan</font></td>
<td align="center" bgcolor="#c74e59" width="226"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Permohonan ditolak</font></td></tr></tbody></table>
                            </ul>
                            <br>
                            
                              <b><u>Tempoh Pembiayaan</u></b> 
                           
                            <ul>
                            <li> Maksimum 10 tahun (120 Bulan) atau sehingga umur persaraan atau yang mana lebih awal. 
                             </li></ul>
              
            <br>
                            
                             

                           
            </div>
                        
                        
          </div>

          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
       
          <b><u>Nisbah Khidmat Hutang / DSR</u></b>  <br> <br>
                           
                           
<table style="BORDER-COLLAPSE: collapse" border="1" cellspacing="1" width="100%">
<tbody>
<tr>
<td rowspan="2" align="center" bgcolor="#e8a74c" width="218"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Kategori Pemohon</font></b></span></td>
<td colspan="2" align="center" bgcolor="#e8a74c" width="337"><span style="LETTER-SPACING: 1pt"><b><font style="FONT-SIZE: 9pt" face="Arial">Kadar DSR</font></b></span></td></tr>
<tr>
<td align="center" bgcolor="#e8a74c" width="269"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial">Pendapatan Bulanan</font></td>
<td align="center" bgcolor="#e8a74c" width="64"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial">DSR</font></td></tr>
<tr>
<td rowspan="4" align="center" bgcolor="#c74e59" width="218">
<p style="LINE-HEIGHT: normal; MARGIN-BOTTOM: 0pt" class="MsoNormal" align="center"><font color="#ffffff"><span style="FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">Kakitangan </span><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">Kerajaan &amp; GLC (Bayaran ansuran BPA &amp; PGM)</span></font></p>
<p></p></td>
<td align="center" bgcolor="#c74e59" width="269">
<p style="TEXT-ALIGN: center; LINE-HEIGHT: normal; MARGIN-BOTTOM: 0pt" class="MsoNormal" align="center"><font color="#ffffff"><span style="FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">&lt; RM2,000.00 </span></font></p><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">(Tertakluk kepada kelulusan Ibu Pejabat)</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">50%</font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">RM2,000.00 – RM3,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt">60%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM3,001.00 – RM 5,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">70%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM 5,001.00 dan ke atas</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">75%</span></font></td></tr>
<tr>
<td rowspan="4" align="center" bgcolor="#c74e59" width="218"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">Kakitangan Swasta &amp; Panel Majikan (Bayaran Ansuran / BPA / PGM / PD Cek / Arahan Tetap)</span></font></td>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM3,000.00 – RM5,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">60%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM5,001.00 – RM7,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">70%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM7,001.00 – RM10,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">75%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><font face="Arial"><span style="LINE-HEIGHT: 115%; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">≥</span></font><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM10,001 ke atas</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">80%</span></font></td></tr>
<tr>
<td rowspan="3" align="center" bgcolor="#c74e59" width="218"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">Kakitangan Swasta Bukan Panel Majikan (Bayaran Ansuran / BPA / PGM / PD Cek / Arahan Tetap)</span></font></td>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM5,001.00 – RM7,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">70%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM7,001.00 – RM10,000.00</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">75%</span></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="269"><font color="#ffffff"><font face="Arial"><span style="LINE-HEIGHT: 115%; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">≥</span></font><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">RM10,001 ke atas</span></font></td>
<td align="center" bgcolor="#c74e59" width="64"><font color="#ffffff"><span style="LINE-HEIGHT: 115%; FONT-FAMILY: Arial; LETTER-SPACING: 1pt; FONT-SIZE: 9pt" lang="FI">80%</span></font></td></tr></tbody></table>

<br>
                            
                              <b><u>Syarat Jaminan</u></b> 
                           
                            <ul>
                            <li> 
    Penjamin / cagaran tidak diperlukan termasuk kakitangan dengan status kontrak bersambung (bagi kakitangan Kemas & Jabatan Perpaduan sahaja) 
                             </li></ul>
                            <br>
                            
                              <b><u>Perlindungan DTT</u></b> 
                           
                            <ul>
                            <li> Skim Takaful Keluarga Berkelompok</li></ul>

                             <br>
                            
                              <b><u>Bayaran Awal / Pendahuluan Pembiayaan </u></b> 
                           
                            <ul>
                            <li> Tiada bayaran awal / pendahuluan untuk kakitangan kerajaan, badan berkanun dan GLC dengan cara bayaran balik melalui potongan BPA.  
                             </li>
                             <li>Untuk lain-lain cara bayaran dan kategori majikan, bayaran awal / pendahuluan 1 bulan diambil / ditolak secara manual dari jumlah pembiayaan dan dikreditkan ke dalam akaun pembiayan yang berkenaan.</li></ul>
              
                            <br>
                            
                              <b><u>Had Pendedahan Pembiayaan </u></b> <br><br>

<table style="BORDER-COLLAPSE: collapse" border="1" cellspacing="1" width="100%">
<tbody>
<tr>
<td align="center" bgcolor="#e8a74c" width="130"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial"><b>Pelanggan</b></font></td>
<td align="center" bgcolor="#e8a74c" width="120"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial"><b>Bilangan Akaun</b></font></td>
<td align="center" bgcolor="#e8a74c"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" face="Arial"><b>Jumlah Pembiayaan</b></font></td></tr>
<tr>
<td align="center" bgcolor="#c74e59" width="130"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">Baru / Sedia Ada</font></td>
<td align="center" bgcolor="#c74e59" width="120"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">3(tiga) Akaun</font></td>
<td align="center" bgcolor="#c74e59"><font style="LETTER-SPACING: 1pt; FONT-SIZE: 9pt" color="#ffffff" face="Arial">RM 200,000.00</font></td></tr></tbody></table>

<br>
                            
                              <b><u>Yuran Proses / Perkhidmatan </u></b> 
                           
                            <ul>
                            <li> Tiada yuran proses untuk pemohon daripada sektor kerajaan, badan berkanun dan GLC.</li> <li>
Yuran proses sebanyak 0.5% untuk kategori majikan swasta.</li> </ul>

<br>
                            
                              <b><u>Tempoh Pengeluaran</u></b> 
                           
                            <ul>
                        <li> Pengeluaran pembiayaan dalam tempoh 3(tiga) hari bekerja selepas penerimaan borang.</li></ul>

                       

      </div>

      

    </div>
          <div clas="row">
                <div class="col-lg-12" align="center">
                <hr>
                    <a class="btn btn-lg btn-danger" href="{{url('/')}}/agent_registration"> <b>Daftar Sebagai Ejen</b></a><hr><br><br><br>
                </div>

          </div>
        </div>

        </div>


        
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>
  

    
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

    $(document).ready(function() {
        
          $("#smart-form-register2").hide();
        $("#smart-form-register").validate({

          // Rules for form validation
          rules : {
              FullName: {
              required : true
            },
            ICNumber : {
              required : true
            },
            
            PhoneNumber: {
                required: true
            },
            Deduction: {
                required: true
            },
            
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true
            },
            Employer: {
                required: true
            },
            
            
            BasicSalary: {
                required: true
                            
            },
            LoanAmount: {
                required: true
            },
            StatusPekerjaan : {
              required:true
            },
            MasaBerkhidmat : {
              required:true
            },
            kemas : {
              required : true
            }
          },

          // Messages for form validation
          messages : {

              FullName: {
              required : 'Please enter your full name'
            },
            
            ICNumber: {
                required: 'Please enter your ic number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number'
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
            Package: {
                required: 'Please select package'
            },
            Employment: {
                required: 'Please select employment type'
            },
            StatusPekerjaan: {
                required: 'Please select employment status'
            },
            MasaBerkhidmat: {
                required: 'Please select employment duration'
            },
            kemas: {
                required: 'Please select Ya atau Tidak'
            },

            Employer: {
                required: 'Please select employer'
            },


            BasicSalary: {
                required: 'Please enter your basic salary'
            },
            LoanAmount: {
                required: 'Please select your loan amount'
            }
          }
        });

      });
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>
<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    

<script type="text/javascript">

 @if (Session::has('employer')) 
 
   $("#majikan2").hide();
    $("#majikan").show();
  @else
   $("#majikan").hide();
   $("#majikan2").show();
  @endif

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();
    if( Employment != '2') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
    }
    else {
         
        $("#majikan2").hide();
         $("#majikan").show();
            
          $("#majikan").simulate('click');
         


    }
  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });

   
});
</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("#Employment option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });



</script>

<script type="text/javascript">

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


         var Employment = getSelectedText('Employment');
         var Employer = getSelectedText('Employer');
          $("#Employment2").val(Employment);
           $("#Employer2").val(Employer); 

</script>
  <script type="text/javascript">

$( ".StatusPekerjaan" ).change(function() {
    var status = $('#StatusPekerjaan').val();
    var employment = $('#Employment').val();

    if(employment==1) { // Kerajaan
        if(status==1) { // Tetap

           $("#kemas").hide();
           $("#masakerja").hide();
           $("#statuspekerjaan").show();
        }
        else if(status==0) { // Tetap

           $("#kemas").show();
           $("#masakerja").hide();
           $("#statuspekerjaan").show();
        }
        else {

            $("#kemas").hide();
            $("#masakerja").hide();
            $("#statuspekerjaan").show();
        }
    }
    else { // Private
         $("#masakerja").show();
         $("#kemas").hide();
         $("#statuspekerjaan").hide();
    }
    

   
});
</script>

<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>
