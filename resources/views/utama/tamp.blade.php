@extends('layouts.main')

@section('content')
    <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('assets/images/kl.jpg');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Global I Exceed Management Sdn.Bhd</h2> 
            <p class="lead mb-5 probootstrap-animate">
              

            <!-- </p>
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Verion</a> 
            </p> -->
          </div> 
          <div class="col-md probootstrap-animate">
            <form action="#" class="probootstrap-form border border-danger">
              <div class="form-group">
                <div class="row mb-3">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Nama Penuh</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-person"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">No Kad Pengenalan</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row mb-5">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Nombor Telefon Bimbit</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-clipboard"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single">Jenis Pekerjaan</label>

                      <label for="id_label_single" style="width: 100%;">
                        <select class="js-example-basic-single js-states form-control" id="id_label_single">
                          <option value="Australia">Kerajaan / Badan Berkanun / GLC</option>
                          <option value="Japan">Swasta Panel Majikan</option>
                          <option value="United States">Swasta Bukan Panel Majikan</option>
                        </select>
                      </label>


                    </div>
                  </div>
                </div>

                <div class="row mb-5">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Majikan</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-briefcase"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single2">Status Pekerjaan</label>
                      <div class="probootstrap_select-wrap">
                        <label for="id_label_single2" style="width: 100%;">
                        <select class="js-example-basic-single js-states form-control" id="id_label_single2" style="width: 100%;">
                          <option value="Australia">Tetap</option>
                          <option value="Japan">Kontrak</option>
                          
                        </select>
                      </label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->

                <div class="row mb-3">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Gaji Asas (RM)</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Elaun (RM)</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row mb-5">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Jumlah Potongan Bulanan Semasa (RM)</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                  
                </div>
                
                <div class="row mb-5">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-departure">Pakej</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md">
                    <div class="form-group">
                      <label for="probootstrap-date-arrival">Jumlah Pembiayaan (RM)</label>
                      <div class="probootstrap-date-wrap">
                        <span class="icon ion-card"></span> 
                        <input type="text" id="" class="form-control" placeholder="">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END row -->
                <div class="row">
                  <div class="col-md">
                    
                  </div>
                  <div class="col-md">
                    <input type="submit" value="Submit" class="btn btn-danger btn-block" style="cursor:pointer;">
                  </div>
                </div>
              </div>
            </form>
          </div>



        </div>
      </div>
    
    </section>
    <!-- END section image -->
    
    <section class="probootstrap_section">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Berita</h2>
          </div>
        </div>
        
        <div class="row probootstrap-animate">
          <div class="col-md-12">
            <div class="owl-carousel js-owl-carousel-2">
              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_4.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_5.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->


              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_2.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_1.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_3.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_4.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->

              <div>
                <div class="media probootstrap-media d-block align-items-stretch mb-4 probootstrap-animate">
                  <img src="assets/images/sq_img_5.jpg" alt="Free Template by ProBootstrap" class="img-fluid">
                  <div class="media-body">
                    <h5 class="mb-3">02. Service Title Here</h5>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                  </div>
                </div>
              </div>
              <!-- END slide item -->
              
            </div>
          </div>
        </div>
      </div>
    </section> <!-- END section -->  

    <section class="probootstrap_section">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Pelayanan Kami</h2>
          </div>
        </div>
      </div>
    </section>
        

    <section class="probootstrap-section-half d-md-flex" id="section-about">
      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>
      <div class="probootstrap-text">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">
          <h2 class="heading mb-4">Konsep Pembiayaan i-Lestari</h2>
          <p>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</p>
        </div>
      </div>
    </section>


    <section class="probootstrap-section-half d-md-flex">
      <div class="probootstrap-image order-2 probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_3.jpg)"></div>
      <div class="probootstrap-text order-1">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInLeft">
          <h2 class="heading mb-4">Kategori Pelanggan </h2>
            <ol>
              <li>Persendirian-i Lestari Awam</li>
              <ol type="i">
                <li>Sektor Kerajaan dan Badan Berkanun berstatus tetap dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan status kontrak bersambung tidak kurang 2 tahun berkhidmat hanya kepada Kakitangan KEMAS dan Jabatan Perpaduan sahaja.</li>
                <li>Anak Syarikat Kerajaan / GLC</li>
              </ol>
              <li>i-Lestari Swasta</li>
              <ol>
                <li>Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)</li>
              </ol>
            </ol>
        </div>
      </div>
    </section>

    <section class="probootstrap-section-half d-md-flex" id="section-about">
      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url(assets/images/img_2.jpg)"></div>
      <div class="probootstrap-text">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">
          <h2 class="heading mb-4">Konsep Pembiayaan i-Lestari</h2>
          <p>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</p>
        </div>
      </div>
    </section><!-- END section -->

    <section class="probootstrap_section" id="section-feature-testimonial">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-12 text-center mb-5 probootstrap-animate">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Pelayanan Kami</h2>
            <div class="row justify-content-center mb-5 probootstrap-animate">
              <div class="col-md-6 text-justify">
                <ol>
                  <h2 class="heading mb-3"><li>Konsep Pembiayaan i-Lestari </li></h2> 
                  <ol type="i">
                    <h5 class="mb-5"><li>Pembiayaan i-Lestari adalah berkonsepkan Tawarruq</li></h5>
                  </ol>
                  
                  <h2 class="heading mb-3"><li>Kategori Pelanggan</li></h2>
                  <ol type="i">
                    <h5><li>Persendirian-i Lestari Awam</li></h5>
                    <p>Sektor Kerajaan dan Badan Berkanun berstatus tetap dengan tempoh berkhidmat melebihi 3 bulan dan kakitangan status kontrak bersambung tidak kurang 2 tahun berkhidmat hanya kepada Kakitangan KEMAS dan Jabatan Perpaduan sahaja. Anak Syarikat Kerajaan / GLC</p>
                    <h5><li>i-Lestari Swasta</li></h5>
                    <p class="mb-5">Sektor Swasta / Syarikat Terpilh (Panel Majikan Dan Bukan Panel Majikan)</p>
                  </ol>

                  <h2 class="heading mb-3"><li>Kadar Keuntungan</li></h2>
                  <ol>
                    <h5><li>Persendirian-i Lestari Awam</li></h5>
                    <ol style="list-style-type:disc">
                      <li>1 - 3 tahun = 4.15% setahun</li>
                      <li class="mb-3">4 - 10 tahun = 4.75% setahun</li>
                    </ol>
                    <h5><li>i-Lestari Swasta Panel Majikan (Kadar Tetap)</li></h5>
                    <ol style="list-style-type:disc">
                      <li>Pendapatan RM 3,000.00 - RM 5,000.00 = 7.00% setahun</li>
                      <li>Pendapatan RM 5,001.00 - RM 10,000.00 = 6.25% setahun</li>
                      <li class="mb-3">Pendapatan > RM 10,001.00 = 6.00% setahun</li>
                    </ol>
                    <h5><li>i-Lestari Swasta Bukan Panel Majikan (Kadar Tetap)</li></h5>
                    <ol style="list-style-type:disc">
                      <li>Pendapatan RM5,001 - RM10,000 = 6.50% Setahun</li>
                      <li class="mb-3">Pendapatan > RM10,000 = 6.25% Setahun</li>
                    </ol>
                    <h4 class="mb-5">(Gaji Kasar = Gaji Pokok + Elaun Tetap)</h4>
                  </ol>

                  <h2 class="heading mb-3"><li>Laporan CCRIS </li></h2>
                  <ol>
                    <h5 class="mb-4"><li>Laporan CCRIS (dari aplikasi 'Credit Bureau') perlu dikemukakan untuk setiap permohonan.</li></h5>
                    
                    <table class="table table-sm table-default">
                      <thead>
                        <tr class="table-danger">
                          <th scope="col">Tunggakan</th>
                          <th scope="col">Kelulusan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td scope="row">1 Bulan</td>
                          <td>Kelulusan secara budibicara</td>
                        </tr>
                        <tr>
                          <td scope="row">2 Bulan</td>
                          <td>Jacob</td>
                        </tr>
                        <tr>
                          <td scope="row">3 Bulan</td>
                          <td>Bulan Permohonan ditolak</td>
                        </tr>
                      </tbody>
                    </table>
                  </ol>

                  <h2 class="heading mb-3"><li>Tempoh Pembiayaan</li></h2>
                  <h2 class="heading mb-3"><li> </li></h2>
     
                </ol>
                
              </div>
              <div class="col-md-6"></div>
            </div>


          </div>
        </div> <!-- end -->
        
      </div>
    </section>
    <!-- END section -->

    <section class="probootstrap_section bg-light">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">News</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_1.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_2.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

          </div>
          <div class="col-md-6">
            
            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_4.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url(assets/images/img_5.jpg)">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section> <!-- END section -->

@endsection